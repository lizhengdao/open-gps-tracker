/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import nl.renedegroot.android.gpstracker.ng.settings.databinding.SettingsActivitySettingsBinding
import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.gpstracker.utils.preference.LoggingSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.viewmodel.lazyViewModel

fun startSettingsActivity(context: Context) {
    val intent = Intent(context, SettingsActivity::class.java)
    context.startActivity(intent)
}

class SettingsActivity : AppCompatActivity() {

    private lateinit var binding: SettingsActivitySettingsBinding
    private val presenter by lazyViewModel {
        SettingsPresenter(
                SettingsViewModel(),
                LoggingSettingsPreferences(this),
                UnitSettingsPreferences(this),
                ServiceCommander(this)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.settings__activity_settings)
        binding.presenter = presenter
        binding.model = presenter.viewModel


        setSupportActionBar(binding.settingsToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }
}
