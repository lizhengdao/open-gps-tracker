/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.v2.wear.communication

import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import nl.renedegroot.android.gpstracker.v2.sharedwear.messaging.MessageListenerService
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatisticsMessage
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatusMessage
import nl.renedegroot.android.gpstracker.v2.wear.ControlActivity

class WearMessageListenerService : MessageListenerService() {

    override fun updateStatus(status: StatusMessage) {
        val intent = ControlActivity.createIntent(this, status)
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun updateStatistics(statistics: StatisticsMessage) {
        val intent = ControlActivity.createIntent(this, statistics)
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}
