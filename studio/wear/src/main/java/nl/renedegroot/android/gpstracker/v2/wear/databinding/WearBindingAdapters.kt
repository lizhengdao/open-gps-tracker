/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.v2.wear.databinding

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import nl.renedegroot.android.gpstracker.utils.formatting.LocaleProvider
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.formatting.asSmallLetterSpans
import nl.renedegroot.android.gpstracker.v2.wear.Control
import nl.renedegroot.android.gpstracker.v2.wear.R

class WearBindingAdapters {

    @BindingAdapter("android:src")
    fun ImageView.setImageResource(resId: Int) {
        setImageResource(resId)
    }

    @BindingAdapter("android:src")
    fun ImageView.setControl(control: Control?) {
        val id = control?.iconId
        if (id != null) {
            setImageResource(id)
            isEnabled = control.enabled
            alpha = if (control.enabled) 1.0F else 0.5F
        } else {
            setImageDrawable(null)
            isEnabled = false
            alpha = 0.5F
        }
    }

    @BindingAdapter("enabled")
    fun SwipeRefreshLayout.setEnables(enabled: Boolean) {
        isEnabled = enabled
    }

    @BindingAdapter("wearDuration")
    fun TextView.setDuration(timeStamp: Long?) {
        if (timeStamp == null || timeStamp <= 0L) {
            text = context.getText(R.string.empty_dash)
        } else {
            text = StatisticsFormatter(LocaleProvider(context), UnitSettingsPreferences(context))
                    .convertSpanToCompactDuration(context, timeStamp)
                    .replace(' ', '\n')
                    .asSmallLetterSpans()
        }
    }

    @BindingAdapter("distance")
    fun TextView.setDistance(distance: Float?) {
        if (distance == null || distance <= 0L) {
            text = context.getText(R.string.empty_dash)
        } else {
            text = StatisticsFormatter(LocaleProvider(context), UnitSettingsPreferences(context))
                    .convertMetersToDistance(context, distance)
                    .replace(' ', '\n')
                    .asSmallLetterSpans()
        }
    }

    @BindingAdapter("speed", "inverse")
    fun TextView.setSpeed(speed: Float?, inverse: Boolean?) {
        if (speed == null || speed <= 0L) {
            text = context.getText(R.string.empty_dash)
        } else {
            text = StatisticsFormatter(LocaleProvider(context), UnitSettingsPreferences(context))
                    .convertMeterPerSecondsToSpeed(context, speed, inverse ?: false)
                    .replace(' ', '\n')
                    .asSmallLetterSpans()

        }
    }
}
