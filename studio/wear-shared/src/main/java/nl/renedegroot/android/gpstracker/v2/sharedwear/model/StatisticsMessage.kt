/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.v2.sharedwear.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.android.gms.wearable.DataMap
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class StatisticsMessage(val speed: Float, val inverse: Boolean, val distance: Float, val duration: Long) : WearMessage(PATH_STATISTICS), Parcelable {

    constructor(dataMap: DataMap) :
            this(dataMap.getFloat(SPEED), dataMap.getBoolean(INVERSE), dataMap.getFloat(DISTANCE), dataMap.getLong(DURATION))

    override fun toDataMap(): DataMap {
        val dataMap = DataMap()
        dataMap.putFloat(SPEED, speed)
        dataMap.putBoolean(INVERSE, inverse)
        dataMap.putFloat(DISTANCE, distance)
        dataMap.putLong(DURATION, duration)
        return dataMap
    }

    companion object {
        const val PATH_STATISTICS = "/ogt-recordings-statistics"

        private const val SPEED = "SPEED"
        private const val INVERSE = "INVERSE"
        private const val DISTANCE = "DISTANCE"
        private const val DURATION = "DURATION"
    }
}
