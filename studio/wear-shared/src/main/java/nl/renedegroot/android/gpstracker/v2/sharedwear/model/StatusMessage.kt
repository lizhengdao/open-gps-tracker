/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.v2.sharedwear.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.android.gms.wearable.DataMap
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class StatusMessage(val status: Status) : WearMessage(PATH_STATUS), Parcelable {
    constructor(dataMap: DataMap) : this(Status.valueOf(dataMap.getInt(STATUS)))

    override fun toDataMap(): DataMap {
        val dataMap = DataMap()
        dataMap.putInt(STATUS, status.code)
        return dataMap
    }

    enum class Status(val code: Int) {
        UNKNOWN(-1),
        START(1),
        PAUSE(2),
        RESUME(3),
        STOP(4);

        companion object {
            @JvmStatic
            fun valueOf(code: Int): Status =
                    when (code) {
                        1 -> START
                        2 -> PAUSE
                        3 -> RESUME
                        4 -> STOP
                        else -> UNKNOWN
                    }
        }
    }

    companion object {
        const val PATH_STATUS = "/ogt-recordings-status"

        private const val STATUS = "STATUS"
    }
}
