/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.base.dagger

import android.content.ContentResolver
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GnnsStatusControllerImpl
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusControllerImpl
import nl.renedegroot.android.gpstracker.ng.base.location.GpsLocationFactory
import nl.renedegroot.android.gpstracker.ng.base.location.LocationFactory
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionChecker
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class SystemBaseModule {

    @Provides
    fun gpsStatusController(application: Context): GpsStatusController =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                GnnsStatusControllerImpl(application)
            } else {
                GpsStatusControllerImpl(application)
            }

    @Provides
    @Singleton
    @Computation
    fun computationExecutor(): Executor = ThreadPoolExecutor(4, 8, 10L, TimeUnit.SECONDS, LinkedBlockingQueue())

    @Provides
    @Singleton
    @DiskIO
    fun diskExecutor(): Executor = ThreadPoolExecutor(1, 2, 10L, TimeUnit.SECONDS, LinkedBlockingQueue())

    @Provides
    @Singleton
    @NetworkIO
    fun networkExecutor(): Executor = ThreadPoolExecutor(1, 16, 30L, TimeUnit.SECONDS, LinkedBlockingQueue())

    @Provides
    @UIUpdate
    fun uiUpdateExecutor(): Executor = Executors.newSingleThreadExecutor()

    @Provides
    fun packageManager(application: Context): PackageManager = application.packageManager

    @Provides
    fun locationFactory(application: Context): LocationFactory = GpsLocationFactory(application)

    @Provides
    fun contentResolver(application: Context): ContentResolver = application.contentResolver

    @Provides
    fun permissionChecker() = PermissionChecker()

}
