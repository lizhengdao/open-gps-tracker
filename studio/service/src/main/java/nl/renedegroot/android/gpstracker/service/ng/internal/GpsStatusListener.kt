/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.Manifest
import android.net.Uri
import androidx.annotation.RequiresPermission
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.service.ng.LoggerStateCallback
import javax.inject.Inject

internal class GpsStatusListener @Inject constructor(
        private val notification: LoggerNotification,
        private val gpsStatusController: GpsStatusController
) : LoggerStateCallback {

    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    override fun didStartLogging(trackUri: Uri) {
        gpsStatusController.startUpdates(statusListener)
    }

    override fun didPauseLogging(trackUri: Uri) {
        gpsStatusController.stopUpdates()
    }

    override fun didStopLogging() {
        gpsStatusController.stopUpdates()
    }

    private val statusListener = object : GpsStatusController.Listener {
        override fun onStartListening() {
            GpsStatus.Connecting(0, 0)
        }

        override fun onChange(usedSatellites: Int, maxSatellites: Int) {
            notification.statusByProvider(GpsStatus.Connecting(usedSatellites, maxSatellites))
        }

        override fun onFirstFix() {
            notification.statusByProvider(GpsStatus.Connected)
        }

        override fun onStopListening() {
            notification.statusByProvider(GpsStatus.Nothing)
        }
    }
}
