/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.Manifest
import android.annotation.TargetApi
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_LOW
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.res.Resources
import android.location.Location
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import nl.renedegroot.android.gpstracker.service.R
import nl.renedegroot.android.gpstracker.service.ng.CommandFactory
import nl.renedegroot.android.gpstracker.service.ng.LoggerStateCallback
import nl.renedegroot.android.gpstracker.utils.VersionHelper
import nl.renedegroot.android.gpstracker.utils.permissions.permissionActivityIntent

private const val NOTIFICATION_CHANNEL_ID = "logger_notification"
private const val ID_STATUS = 564564
private const val ID_DISABLED = ID_STATUS + 1
private val SMALL_ICON = R.drawable.ic_maps_indicator_current_position

internal class LoggerNotification(
        private val notificationManager: NotificationManager,
        private val resources: Resources,
        private val service: Service,
        private val commandFactory: CommandFactory
) : LoggerStateCallback {

    private var status: NotificationState = NotificationState.Stopped(GpsStatus.Nothing)

    init {
        if (VersionHelper().isAtLeast(Build.VERSION_CODES.O)) {
            createChannel()
        }
    }

    // region Gps Status

    fun statusByProvider(gpsStatus: GpsStatus) {
        status.gpsStatus = gpsStatus
        updateNotification()
    }

    // endregion

    // region LoggerStateCallback

    override fun didStartLogging(trackUri: Uri) {
        status = NotificationState.Logging(trackUri, status.gpsStatus)
        updateNotification()
    }

    override fun didPauseLogging(trackUri: Uri) {
        status = NotificationState.Paused(trackUri, status.gpsStatus)
        updateNotification()
    }

    override fun didStopLogging() {
        status = NotificationState.Stopped(status.gpsStatus)
        notificationManager.cancel(ID_STATUS)
    }

    private fun updateNotification() {
        if (status is NotificationState.Stopped) {
            return
        }

        val builder = NotificationCompat.Builder(service, NOTIFICATION_CHANNEL_ID)
                .setOnlyAlertOnce(true)
                .setSmallIcon(SMALL_ICON)
                .setContentTitle(resources.getString(R.string.service_title))
                .setOngoing(true)
        when (val status = this.status) {
            is NotificationState.Logging -> {
                builder.setContentIntent(contentIntent(status.trackUri))
                builder.setContentText(status.gpsStatus.statusDescription(resources))
                addPauseButton(builder)
                service.startForeground(ID_STATUS, builder.build())
            }
            is NotificationState.Paused -> {
                builder.setContentIntent(contentIntent(status.trackUri))
                builder.setContentText(status.gpsStatus.statusDescription(resources))
                addResume(builder)
                notificationManager.notify(ID_STATUS, builder.build())
            }
        }
    }

    private fun contentIntent(trackUri: Uri): PendingIntent? {
        val notificationIntent = Intent(Intent.ACTION_VIEW, trackUri).also {
            it.flags = FLAG_ACTIVITY_NEW_TASK
        }
        return PendingIntent.getActivity(service, 0, notificationIntent, 0)
    }

    private fun addPauseButton(builder: NotificationCompat.Builder) {
        val intent = commandFactory.createPauseLoggerCommand()
        val pendingIntent = PendingIntent.getService(service, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.addAction(R.drawable.ic_pause_24dp, resources.getString(R.string.logcontrol_pause), pendingIntent)
    }

    private fun addResume(builder: NotificationCompat.Builder) {
        val intent = commandFactory.createResumeLoggerCommand()
        val pendingIntent = PendingIntent.getService(service, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.addAction(R.drawable.ic_play_arrow_24dp, resources.getString(R.string.logcontrol_resume), pendingIntent)
    }

    // endregion

    fun blockedByPermission(command: Intent) {
        val pendingRestartRequest = PendingIntent.getService(service, 0, command, 0)
        val requestIntent = permissionActivityIntent(
                service,
                arrayListOf(Manifest.permission.ACCESS_FINE_LOCATION),
                pendingRestartRequest
        ).also {
            it.flags = FLAG_ACTIVITY_NEW_TASK
        }
        val pendingRequestIntent = PendingIntent.getActivity(service, 0, requestIntent, 0)
        val builder = NotificationCompat.Builder(service, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(SMALL_ICON)
                .setAutoCancel(true)
                .setContentTitle(resources.getString(R.string.service_title))
                .setContentText(resources.getString(R.string.service_permission_required))
                .setTicker(resources.getString(R.string.service_title))
                .setContentIntent(pendingRequestIntent)
        val notification = builder.build()
        service.startForeground(ID_STATUS, notification)
        leaveForeground()
    }

    fun blockedByDisabledProvider() {
        val contentTitle = resources.getString(R.string.service_title)
        val contentText = resources.getString(R.string.service_gpsdisabled)
        val tickerText = resources.getString(R.string.service_gpsdisabled)

        val builder = NotificationCompat.Builder(service, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(SMALL_ICON)
                .setAutoCancel(true)
                .setTicker(tickerText)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
        notificationManager.notify(ID_DISABLED, builder.build())
    }

    fun unblockedByDisabledProvider() {
        notificationManager.cancel(ID_DISABLED)
    }

    fun leaveForeground() {
        service.stopForeground(false)
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        if (notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    resources.getString(R.string.notification_operation_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT)
            channel.lockscreenVisibility = VISIBILITY_PUBLIC
            channel.name = resources.getString(R.string.notification_operation_channel_name)
            channel.description = resources.getString(R.string.notification_operation_channel_description)
            channel.enableLights(false)
            channel.enableVibration(false)
            channel.importance = IMPORTANCE_LOW
            notificationManager.createNotificationChannel(channel)
        }
    }
}

sealed class NotificationState(var gpsStatus: GpsStatus) {
    class Logging(val trackUri: Uri, gpsStatus: GpsStatus) : NotificationState(gpsStatus)
    class Paused(val trackUri: Uri, gpsStatus: GpsStatus) : NotificationState(gpsStatus)
    class Stopped(gpsStatus: GpsStatus) : NotificationState(gpsStatus)
}

sealed class GpsStatus {
    object Nothing : GpsStatus()
    object Connected : GpsStatus()
    data class Connecting(val usedSatellites: Int, val maxSatellites: Int) : GpsStatus()
    data class ReceivedLocation(val location: Location) : GpsStatus()
}

fun GpsStatus.statusDescription(resources: Resources): String =
        when (this) {
            GpsStatus.Nothing -> resources.getString(R.string.service_started)
            is GpsStatus.Connecting -> resources.getString(R.string.service_connecting, usedSatellites, maxSatellites)
            GpsStatus.Connected -> resources.getString(R.string.service_acquired)
            is GpsStatus.ReceivedLocation -> resources.getString(R.string.service_location, location.accuracy)
        }
