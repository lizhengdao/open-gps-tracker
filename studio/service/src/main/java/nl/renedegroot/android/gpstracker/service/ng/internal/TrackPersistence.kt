/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.content.ContentResolver
import android.content.ContentValues
import android.location.Location
import android.net.Uri
import androidx.annotation.RequiresPermission
import androidx.core.content.contentValuesOf
import nl.renedegroot.android.gpstracker.service.BuildConfig
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.ACCURACY
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.ALTITUDE
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.BEARING
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.LATITUDE
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.LONGITUDE
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.SPEED
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.TIME
import nl.renedegroot.android.gpstracker.service.util.segmentsUri
import nl.renedegroot.android.gpstracker.service.util.tracksUri
import nl.renedegroot.android.gpstracker.service.util.waypointsUri

internal class TrackPersistence(
        private val contextResolver: ContentResolver
) {

    fun createTrack() =
            checkNotNull(contextResolver.insert(tracksUri(), ContentValues(0))) { "Must be able to create track" }

    fun createSegment(trackId: Long) =
            checkNotNull(contextResolver.insert(segmentsUri(trackId), ContentValues(0))) { "Must be able to create segment" }

    fun createWaypoint(trackId: Long, segmentId: Long, location: Location): Uri {
        val locationContentValues = contentValuesOf(
                LATITUDE to location.latitude,
                LONGITUDE to location.longitude,
                TIME to location.time,
                SPEED to location.speed
        ).also {
            if (location.hasAccuracy()) {
                it.put(ACCURACY, location.accuracy)
            }
            if (location.hasAltitude()) {
                it.put(ALTITUDE, location.altitude)
            }
            if (location.hasBearing()) {
                it.put(BEARING, location.bearing)
            }
        }
        return checkNotNull(contextResolver.insert(waypointsUri(trackId, segmentId), locationContentValues)) { "Must be able to create waypoint" }
    }
}
