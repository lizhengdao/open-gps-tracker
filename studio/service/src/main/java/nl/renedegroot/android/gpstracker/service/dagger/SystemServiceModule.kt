package nl.renedegroot.android.gpstracker.service.dagger

import android.content.Context
import android.location.LocationManager
import android.os.Build
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GnnsStatusControllerImpl
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusControllerImpl
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsListener
import nl.renedegroot.android.gpstracker.service.ng.internal.SystemGpsListener
import nl.renedegroot.android.gpstracker.service.ng.internal.LocationFilter
import nl.renedegroot.android.gpstracker.service.ng.internal.LoggerNotification
import nl.renedegroot.android.gpstracker.service.ng.internal.PowerManager
import nl.renedegroot.android.gpstracker.service.ng.internal.ServicePermissionChecker
import nl.renedegroot.android.gpstracker.service.ng.internal.StatePersistence
import nl.renedegroot.android.gpstracker.service.ng.internal.StatePersistenceInterface
import nl.renedegroot.android.gpstracker.utils.preference.LoggingSettingsPreferences

@Module
class SystemServiceModule(val context: Context) {

    @Provides
    internal fun gpsStatusControllerFactory(): GpsStatusController =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                GnnsStatusControllerImpl(context)
            } else {
                GpsStatusControllerImpl(context)
            }

    @Provides
    internal fun gpsListener(
            locationFilter: LocationFilter,
            notification: LoggerNotification,
            powerManager: PowerManager,
            permissionChecker: ServicePermissionChecker
    ): GpsListener {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return SystemGpsListener(locationManager, locationFilter, notification, powerManager, permissionChecker)
    }

    @Provides
    internal fun provideLoggingSettingsPreferences() = LoggingSettingsPreferences(context)

    @Provides
    internal fun statePersistence(): StatePersistenceInterface =
            StatePersistence {
                context.getSharedPreferences("LoggerStatePersistence", Context.MODE_PRIVATE)
            }
}
