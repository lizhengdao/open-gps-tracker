/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.annotation.SuppressLint
import android.location.Location
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import nl.renedegroot.android.gpstracker.service.dagger.ServiceScope
import nl.renedegroot.android.gpstracker.service.ng.CommandFactory
import nl.renedegroot.android.gpstracker.service.ng.LoggerStateCallback
import nl.renedegroot.android.gpstracker.service.util.trackUri
import nl.renedegroot.android.gpstracker.service.util.updateName
import nl.renedegroot.android.gpstracker.utils.preference.clampInterval
import timber.log.Timber
import java.util.concurrent.CopyOnWriteArraySet
import javax.inject.Inject

@ServiceScope
internal class GpsLogger @Inject constructor(
        private val trackPersistence: TrackPersistence,
        private val statePersistence: StatePersistenceInterface,
        private val gpsListener: GpsListener,
        gpsStatusListener: GpsStatusListener,
        notification: LoggerNotification
) {
    private val callbacks = CopyOnWriteArraySet<LoggerStateCallback>()
    internal var logState: LogState = LogState.Stopped

    init {
        addObserver(gpsStatusListener)
        addObserver(notification)
        addObserverNamed("nl.renedegroot.android.gpstracker.ng.features.wear.WearLoggingStateBroadcastReceiver")
        addObserverNamed("nl.renedegroot.android.gpstracker.ng.features.activityrecognition.ActivityRecognizerLoggingBroadcastReceiver")
    }

    @SuppressLint("MissingPermission")
    fun startLogging(trackName: String, interval: Int) {
        Timber.v("moving from $logState to startLogging")
        val track = trackPersistence.createTrack()
        track.updateName(trackName)
        val trackId = checkNotNull(track.lastPathSegment).toLong()
        val segment = trackPersistence.createSegment(trackId)
        val segmentId = checkNotNull(segment.lastPathSegment).toLong()
        val logState = LogState.Logging(trackId, segmentId, null, clampInterval(interval))
        moveToLogState(logState)
    }

    fun updateInterval(interval: Int) {
        Timber.v("during $logState updating interval to $interval")
        logState.let {
            if (it is LogState.Logging && it.interval != interval) {
                this.logState = LogState.Logging(it.trackId, it.segmentId, it.lastWaypoint, interval)
                gpsListener.startListener(this@GpsLogger, interval)
            }
        }
    }

    fun pauseLogging() {
        Timber.v("moving from $logState to pauseLogging")
        logState.let { oldState ->
            if (oldState is LogState.Logging) {
                val logState = LogState.Paused(oldState.trackId, oldState.segmentId, oldState.lastWaypoint, oldState.interval)
                moveToLogState(logState)
            } else {
                Timber.e("Ignoring pause without active logging in state $logState")
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun resumeLogging() {
        Timber.v("moving from $logState to resumeLogging")
        logState.let { oldState ->
            if (oldState is LogState.Paused) {
                val segmentId = checkNotNull(trackPersistence.createSegment(oldState.trackId).lastPathSegment).toLong()
                val logState = LogState.Logging(oldState.trackId, segmentId, oldState.lastWaypoint, oldState.interval)
                moveToLogState(logState)
            } else {
                Timber.e("Ignoring resume without paused logging in state $logState")
            }
        }
    }

    fun stopLogging() {
        Timber.v("moving from $logState to stopLogging")
        moveToLogState(LogState.Stopped)
    }

    @SuppressLint("MissingPermission")
    fun logLocation(location: Location) {
        val logState = logState
        if (logState is LogState.Logging) {
            logState.lastWaypoint = trackPersistence.createWaypoint(logState.trackId, logState.segmentId, location)
        }
    }

    fun addObserver(callback: LoggerStateCallback) {
        Timber.v("addObserver $callback")
        callbacks.add(callback)
    }

    private fun addObserverNamed(className: String) {
        try {
            val clazz = Class.forName(className)
            addObserver(clazz.newInstance() as LoggerStateCallback)
        } catch (e: ClassNotFoundException) {
            Timber.e(e, "Failed to load logger observer $className")
        }
    }

    fun removeObserver(callback: LoggerStateCallback) {
        Timber.v("removeObserver $callback")
        callbacks.remove(callback)
    }

    fun saveState() {
        Timber.v("saveState $logState")
        statePersistence.saveLogState(logState)
    }

    fun loadState() {
        Timber.v("loadState $logState")
        moveToLogState(statePersistence.loadLogState())
    }

    private fun moveToLogState(logState: LogState) {
        this.logState = logState
        Timber.v("Moved to log state $logState")
        runBlocking(context = Dispatchers.Main) {
            when (logState) {
                is LogState.Logging -> {
                    val didStart = withContext(Dispatchers.IO) {
                        gpsListener.startListener(this@GpsLogger, logState.interval)
                    }
                    if (didStart) {
                        callbacks.forEach {
                            it.didStartLogging(trackUri(logState.trackId))
                        }
                    }
                }
                is LogState.Paused -> callbacks.forEach {
                    withContext(Dispatchers.IO) {
                        gpsListener.stopListener()
                    }
                    it.didPauseLogging(trackUri(logState.trackId))
                }
                LogState.Stopped -> callbacks.forEach {
                    withContext(Dispatchers.IO) {
                        gpsListener.stopListener()
                    }
                    it.didStopLogging()
                }
            }
        }
    }
}

