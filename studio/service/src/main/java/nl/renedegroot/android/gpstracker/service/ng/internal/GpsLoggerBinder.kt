/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.net.Uri
import android.os.RemoteException
import nl.renedegroot.android.gpstracker.integration.IGPSLoggerServiceRemote
import nl.renedegroot.android.gpstracker.integration.IGPSLoggerStateCallback
import nl.renedegroot.android.gpstracker.service.ng.LoggerStateCallback
import nl.renedegroot.android.gpstracker.service.util.waypointUri
import timber.log.Timber

internal class GpsLoggerBinder(private val gpsLogger: GpsLogger) : IGPSLoggerServiceRemote.Stub() {

    override fun loggingState(): Int = gpsLogger.logState.code

    override fun getTrackId() = when (val state = gpsLogger.logState) {
        LogState.Stopped -> -1
        is LogState.Paused -> state.trackId
        is LogState.Logging -> state.trackId
    }

    override fun getLastWaypoint(): Uri? = when (val state = gpsLogger.logState) {
        LogState.Stopped -> waypointUri(-1, -1, -1)
        is LogState.Paused -> state.lastWaypoint
        is LogState.Logging -> state.lastWaypoint
    }

    override fun addObserver(callback: IGPSLoggerStateCallback) {
        gpsLogger.addObserver(LoggerStateCallbackWrapper(gpsLogger, callback))
    }

    override fun removeObserver(callback: IGPSLoggerStateCallback) {
        gpsLogger.removeObserver(LoggerStateCallbackWrapper(gpsLogger, callback))
    }
}

private class LoggerStateCallbackWrapper(
        private val gpsLogger: GpsLogger,
        private val callback: IGPSLoggerStateCallback) : LoggerStateCallback {
    override fun didStartLogging(trackUri: Uri) {
        try {
            callback.didStartLogging(trackUri)
        } catch (e: RemoteException) {
            Timber.w(e, "Failed to update didStartLogging")
            gpsLogger.removeObserver(this)
        }
    }

    override fun didPauseLogging(trackUri: Uri) {
        try {
            callback.didPauseLogging(trackUri)
        } catch (e: RemoteException) {
            Timber.w(e, "Failed to update didPauseLogging")
            gpsLogger.removeObserver(this)
        }
    }

    override fun didStopLogging() {
        try {
            callback.didStopLogging()
        } catch (e: RemoteException) {
            Timber.w(e, "Failed to update didStopLogging")
            gpsLogger.removeObserver(this)
        }
    }
}
