/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.net.Uri
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_LOGGING
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_PAUSED
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_STOPPED

internal sealed class LogState {
    abstract val code: Int

    internal data class Logging(
            val trackId: Long,
            val segmentId: Long,
            var lastWaypoint: Uri?,
            val interval: Int
    ) : LogState() {

        override val code = STATE_LOGGING
    }

    internal data class Paused(
            val trackId: Long,
            val segmentId: Long,
            val lastWaypoint: Uri?,
            val interval: Int
    ) : LogState() {

        override val code = STATE_PAUSED
    }

    internal object Stopped : LogState() {
        override val code = STATE_STOPPED
    }
}
