/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.service.mock

import android.location.Location
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.service.mock.MockControl.pauseWaypointGenerations
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsListener
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsLogger
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsStatus
import nl.renedegroot.android.gpstracker.service.ng.internal.LoggerNotification
import nl.renedegroot.android.gpstracker.service.ng.internal.PowerManager
import nl.renedegroot.android.gpstracker.service.ng.internal.ServicePermissionChecker
import timber.log.Timber
import java.util.Date
import kotlin.math.cos
import kotlin.math.sin

private const val FINE_INTERVAL = 1000L

internal class MockGpsListener(
        private val notification: LoggerNotification,
        private val powerManager: PowerManager,
        private val permissionChecker: ServicePermissionChecker
) : GpsListener {

    private var scope: CoroutineScope? = null
    private var gpsLogger: GpsLogger? = null
    private var waypointId = 100L

    override fun startListener(gpsLogger: GpsLogger, interval: Int): Boolean {
        this.gpsLogger = gpsLogger
        return permissionChecker.check {
            Timber.d("Requesting location updates")
            scope = CoroutineScope(Dispatchers.Default)
            scope?.launch {
                while (isActive) {
                    if (!pauseWaypointGenerations) {
                        recordNewWaypoint()
                    }
                    delay(interval.toTimeMillis())
                }
            }
            powerManager.updateWakeLock(5 * FINE_INTERVAL)
        }
    }

    override fun stopListener() {
        scope?.cancel()
        scope = null
        powerManager.release()
        notification.leaveForeground()
    }

    private fun recordNewWaypoint() {
        waypointId++
        val amplitude = 0.0001 + waypointId / 10000.0
        val angularSpeed = 5.0
        val lat = 51.2605159 + amplitude * cos(Math.toRadians(waypointId.toDouble() * angularSpeed))
        val lon = 4.2301078 + amplitude * sin(Math.toRadians(waypointId.toDouble() * angularSpeed))

        val location = Location("MockGpsListener").apply {
            accuracy = 4.56F
            latitude = lat
            longitude = lon
            time = Date().time
        }
        notification.statusByProvider(GpsStatus.ReceivedLocation(location))
        gpsLogger?.logLocation(location)
    }
}

object MockControl {
    var pauseWaypointGenerations: Boolean = false
}

private fun Int.toTimeMillis(): Long = 1000L * this
