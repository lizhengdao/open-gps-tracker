/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target.content

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import nl.renedegroot.android.gpstracker.utils.preference.StringPreferenceDelegate
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import nl.renedegroot.opengpstracker.exporter.internal.target.Callback
import nl.renedegroot.opengpstracker.exporter.internal.target.Connection
import nl.renedegroot.opengpstracker.exporter.internal.target.Executable
import nl.renedegroot.opengpstracker.exporter.internal.target.ExportTarget

private const val REQUEST_EXTERNAL_ACCESS = 12344

internal class ContentTarget(contentPreference: StringPreferenceDelegate) : ExportTarget {

    private var contentUri by contentPreference

    override fun checkConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val target = contentUri.asUri()
        if (activity.contentResolver.persistedUriPermissions.any { it.uri == target && it.isWritePermission }) {
            contentUri = target.toString()
            onConnected.invoke(Connection.Success)
        }
    }

    override fun requestConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        activity.registerActivityResult(REQUEST_EXTERNAL_ACCESS) { resultCode, resultIntent ->
            val intentUri = resultIntent?.data
            if (resultCode == Activity.RESULT_OK && intentUri != null) {
                activity.contentResolver.takePersistableUriPermission(
                        intentUri,
                        Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )
                this.contentUri = intentUri.toString()
                onConnected.invoke(Connection.Success)
            } else {
                onConnected.invoke(Connection.Failure)
            }
        }
        activity.startActivityForResult(intent, REQUEST_EXTERNAL_ACCESS)
    }

    override fun removeConnection(activity: ExportActivity) {
        activity.contentResolver.releasePersistableUriPermission(checkNotNull(contentUri.asUri()), Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        contentUri = null
    }


    override fun createUploadTask(contentResolver: ContentResolver, trackUri: Uri, callback: Callback): Executable =
            DocumentUploadTask(contentResolver, trackUri, callback, checkNotNull(contentUri.asUri()))
}

private fun String?.asUri(): Uri? =
        if (this == null) {
            null
        } else {
            Uri.parse(this)
        }