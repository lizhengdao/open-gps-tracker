/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target.drive

import android.content.ContentResolver
import android.net.Uri
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.drive.DriveFile
import com.google.android.gms.drive.DriveFolder
import com.google.android.gms.drive.DriveResourceClient
import com.google.android.gms.drive.MetadataBuffer
import com.google.android.gms.drive.MetadataChangeSet
import com.google.android.gms.drive.query.Filters
import com.google.android.gms.drive.query.Query
import com.google.android.gms.drive.query.SearchableField
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import nl.renedegroot.opengpstracker.exporter.GpxCreator
import nl.renedegroot.opengpstracker.exporter.internal.target.Callback
import nl.renedegroot.opengpstracker.exporter.internal.target.Executable
import timber.log.Timber
import java.util.concurrent.ThreadPoolExecutor

private const val FOLDER_NAME = "Open GPS Tracker - Exports"
private const val FOLDER_MIME = "application/vnd.google-apps.folder"

/**
 * Async task that uses the GpxCreate URI to Stream capability to fill a Google Drive file with said stream
 */
internal class DriveUploadTask(
        contentResolver: ContentResolver,
        private val trackUri: Uri,
        private val callback: Callback,
        private val resourceClient: DriveResourceClient
) : Executable {

    private val gpxCreator = GpxCreator(contentResolver, trackUri)
    private var isCancelled: Boolean = false
    private val filename: String by lazy { GpxCreator.fileName(trackUri, "gpx") }


    override fun executeOn(executor: ThreadPoolExecutor) {
        executor.execute {
            try {
                doInBackground()
                if (!isCancelled) {
                    callback.onFinished(trackUri)
                }
            } catch (exception: ApiException) {
                val localizedMessage = exception.localizedMessage
                val message = if (localizedMessage.isNullOrBlank()) {
                    localizedMessage
                } else {
                    "Google Drive API seems to be unavailable"
                }
                callback.onError(message)
            }
        }
    }

    fun cancel() {
        isCancelled = true
        callback.onCancel()

    }

    private fun doInBackground() {
        Timber.d("Looking for export folder")
        val rootFolder = processResult(resourceClient.rootFolder) ?: return
        if (isCancelled) {
            return
        }
        val query = Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, FOLDER_NAME))
                .addFilter(Filters.eq(SearchableField.MIME_TYPE, FOLDER_MIME))
                .build()
        val rootQueryResult = processResult(rootFolder.let {
            resourceClient.queryChildren(it, query)
        }) ?: return
        Timber.d("Searched for export folder: $rootQueryResult")
        if (isCancelled) {
            return
        }

        val exportFolder: DriveFolder
        val qpxQueryResult: MetadataBuffer?
        if (rootQueryResult.count > 0) {
            Timber.d("Found export folder")
            val folderId = rootQueryResult.get(0).driveId
            exportFolder = folderId.asDriveFolder()
            Timber.d("Have export folder $exportFolder")

            Timber.d("Looking for GPX file")
            val fileQuery = Query.Builder()
                    .addFilter(Filters.eq(SearchableField.TITLE, filename))
                    .addFilter(Filters.eq(SearchableField.MIME_TYPE, GpxCreator.MIME_TYPE_GPX))
                    .build()
            qpxQueryResult = processResult(resourceClient.queryChildren(exportFolder, fileQuery))
                    ?: return
            Timber.d("Searched for GPX file: $qpxQueryResult")
            if (isCancelled) {
                return
            }
        } else {
            qpxQueryResult = null
            Timber.d("Creating export folder")
            val metadata = MetadataChangeSet.Builder()
                    .setTitle(FOLDER_NAME)
                    .build()
            exportFolder = processResult(resourceClient.createFolder(rootFolder, metadata))
                    ?: return
            if (isCancelled) {
                return
            }
            Timber.d("Created export folder $exportFolder")
        }
        rootQueryResult.release()


        if (qpxQueryResult == null || qpxQueryResult.count == 0) {
            Timber.d("Creating GPX file")
            val contents = processResult(resourceClient.createContents()) ?: return
            if (isCancelled) {
                return
            }
            Timber.d("Creating GPX content")
            gpxCreator.createGpx(contents.outputStream)
            if (isCancelled) {
                return
            }
            Timber.d("Created GPX content $contents")
            val metadata = MetadataChangeSet.Builder()
                    .setTitle(filename)
                    .setMimeType(GpxCreator.MIME_TYPE_GPX)
                    .build()
            val fileResult = processResult(resourceClient.createFile(exportFolder, metadata, contents))
                    ?: return
            if (isCancelled) {
                return
            }
            Timber.d("Creating GPX file $fileResult")
        } else {
            Timber.d("Found GPX file $qpxQueryResult")
            val fileId = qpxQueryResult.get(0).driveId
            val exportFile = fileId.asDriveFile()
            val openFile = processResult(resourceClient.openFile(exportFile, DriveFile.MODE_WRITE_ONLY))
                    ?: return
            qpxQueryResult.release()
            Timber.d("Have GPX file $openFile")
            Timber.d("Overwrite GPX content")
            gpxCreator.createGpx(openFile.outputStream)
            val writeResult = processResult(resourceClient.commitContents(openFile, null)) ?: return
            if (isCancelled) {
                return
            }
            Timber.d("Overwritten GPX content $writeResult")
        }

        return
    }

    private fun <T> processResult(resultTask: Task<T>): T? {
        val result = Tasks.await(resultTask)
        return if (resultTask.isSuccessful) {
            result
        } else {
            isCancelled = true
            callback.onError(resultTask.exception?.message ?: "Missing error description")
            null
        }
    }

}

