/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target.mock

import android.content.ContentResolver
import android.net.Uri
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import nl.renedegroot.opengpstracker.exporter.internal.target.Callback
import nl.renedegroot.opengpstracker.exporter.internal.target.Connection
import nl.renedegroot.opengpstracker.exporter.internal.target.Executable
import nl.renedegroot.opengpstracker.exporter.internal.target.ExportTarget
import java.util.concurrent.ThreadPoolExecutor

internal class MockTarget : ExportTarget {

    private var isConnected = false

    override fun checkConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        if (isConnected) {
            onConnected(Connection.Success)
        }
    }

    override fun requestConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        isConnected = true
        onConnected(Connection.Success)
    }

    override fun removeConnection(activity: ExportActivity) {
        isConnected = false
    }


    override fun createUploadTask(contentResolver: ContentResolver, trackUri: Uri, callback: Callback) = object : Executable {
        override fun executeOn(executor: ThreadPoolExecutor) {
            executor.execute {
                // Mocked export action
                callback.onFinished(trackUri)
            }
        }
    }
}