/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.opengpstracker.exporter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import nl.renedegroot.android.gpstracker.utils.viewmodel.viewModel
import nl.renedegroot.opengpstracker.exporter.databinding.ExporterFragmentExportBinding
import nl.renedegroot.opengpstracker.exporter.internal.ExportPresenter
import nl.renedegroot.opengpstracker.exporter.internal.ExportViewModel
import nl.renedegroot.opengpstracker.exporter.internal.Navigation
import nl.renedegroot.opengpstracker.exporter.internal.Navigator
import nl.renedegroot.opengpstracker.exporter.internal.target.ExportProcess
import nl.renedegroot.opengpstracker.exporter.internal.target.createContextTarget
import nl.renedegroot.opengpstracker.exporter.internal.target.createGoogleDriveTarget

class ExportFragment : Fragment() {

    private val navigation = Navigation()
    private val viewModel = ExportViewModel()
    private val presenter: ExportPresenter
        get() = viewModel {
            ExportPresenter(
                    viewModel,
                    navigation,
                    ExportProcess(requireContext().contentResolver, resources),
                    createGoogleDriveTarget(),
                    createContextTarget(requireContext())
            )
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<ExporterFragmentExportBinding>(
                inflater,
                R.layout.exporter__fragment_export,
                container,
                false)
        binding.presenter = presenter
        binding.model = viewModel
        Navigator(requireActivity() as ExportActivity, presenter)
                .observe(viewLifecycleOwner, navigation)

        return binding.root
    }
}
