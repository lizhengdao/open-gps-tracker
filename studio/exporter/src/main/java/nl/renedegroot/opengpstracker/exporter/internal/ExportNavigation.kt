/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.opengpstracker.exporter.internal

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import nl.renedegroot.opengpstracker.exporter.internal.target.ExportTarget

internal class Navigation {

    val navigation = MutableLiveData<Consumable<ExportNavigation>>()

    fun showFailureMessage(message: String) {
        navigation.postValue(Consumable(ExportNavigation.ExportFailed(message)))
    }

    fun checkDriveConnection() {
        navigation.postValue(Consumable(ExportNavigation.DriveCheck))
    }

    fun connectToDrive() {
        navigation.postValue(Consumable(ExportNavigation.DriveConnect))
    }

    fun checkContentConnection() {
        navigation.postValue(Consumable(ExportNavigation.ContentCheck))
    }


    fun connectToContent() {
        navigation.postValue(Consumable(ExportNavigation.ContentConnect))
    }

    fun cancel() {
        navigation.postValue(Consumable(ExportNavigation.Cancel))
    }

    fun done() {
        navigation.postValue(Consumable(ExportNavigation.Done))
    }

    fun showConnectFailureMessage() {
        navigation.postValue(Consumable(ExportNavigation.ConnectionFailed))
    }

    fun disconnectDrive() {
        navigation.postValue(Consumable(ExportNavigation.DriveDisconnect))
    }

    fun disconnectUri() {
        navigation.postValue(Consumable(ExportNavigation.ContentDisconnect))
    }
}

internal class Navigator(
        private val activity: ExportActivity,
        private val presenter: ExportPresenter
) {
    private val googleDriveTarget: ExportTarget = presenter.googleDriveTarget
    private val contentTarget: ExportTarget = presenter.contentTarget

    fun observe(owner: LifecycleOwner, navigation: Navigation) {
        navigation.navigation.observe(owner, { consumable ->
            consumable.consume { destination ->
                navigate(destination)
            }
        })
    }

    private fun navigate(destination: ExportNavigation) = when (destination) {
        ExportNavigation.ConnectionFailed -> driveConnectionFailed()
        is ExportNavigation.ExportFailed -> exportFailed(destination)
        ExportNavigation.Cancel -> cancel()
        ExportNavigation.Done -> activity.finish()
        ExportNavigation.DriveConnect -> googleDriveTarget.requestConnection(activity) {
            presenter.didConnect(it)
        }
        ExportNavigation.ContentConnect -> contentTarget.requestConnection(activity) {
            presenter.didConnect(it)
        }
        ExportNavigation.DriveCheck -> checkDrive()
        is ExportNavigation.ContentCheck -> checkContent()
        ExportNavigation.DriveDisconnect -> googleDriveTarget.removeConnection(activity)
        is ExportNavigation.ContentDisconnect -> contentTarget.removeConnection(activity)
    }

    private fun cancel() {
        AlertDialog.Builder(activity)
                .setTitle("Interrupt")
                .setMessage("Interrupt and stop current export?")
                .setOnCancelListener { }
                .setPositiveButton(android.R.string.ok) { _, _ -> presenter.onCancel() }
                .setNegativeButton(android.R.string.cancel) { _, _ -> }
                .create()
                .show()
    }

    private fun exportFailed(destination: ExportNavigation.ExportFailed) {
        AlertDialog.Builder(activity)
                .setTitle("Exported failed")
                .setMessage(destination.message)
                .setCancelable(true)
                .create()
                .show()
    }

    private fun driveConnectionFailed() {
        AlertDialog.Builder(activity)
                .setTitle("Drive connection failed")
                .setMessage("Authorisation request did not result in a working connection.")
                .setCancelable(true)
                .create()
                .show()
    }

    private fun checkDrive() {
        googleDriveTarget.checkConnection(activity) { presenter.didConnect(it) }
    }

    private fun checkContent() {
        contentTarget.checkConnection(activity) { presenter.didConnect(it) }
    }
}

internal sealed class ExportNavigation {
    object DriveCheck : ExportNavigation()
    object DriveConnect : ExportNavigation()
    object DriveDisconnect : ExportNavigation()
    object ContentCheck : ExportNavigation()
    object ContentConnect : ExportNavigation()
    object ContentDisconnect : ExportNavigation()
    object ConnectionFailed : ExportNavigation()
    class ExportFailed(val message: String) : ExportNavigation()
    object Cancel : ExportNavigation()
    object Done : ExportNavigation()
}
