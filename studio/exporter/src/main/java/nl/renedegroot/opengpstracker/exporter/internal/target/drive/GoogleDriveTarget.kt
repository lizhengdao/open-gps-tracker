/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target.drive

import android.app.Activity
import android.content.ContentResolver
import android.net.Uri
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.drive.Drive
import com.google.android.gms.drive.DriveResourceClient
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import nl.renedegroot.opengpstracker.exporter.internal.target.Callback
import nl.renedegroot.opengpstracker.exporter.internal.target.Connection
import nl.renedegroot.opengpstracker.exporter.internal.target.Executable
import nl.renedegroot.opengpstracker.exporter.internal.target.ExportTarget

private const val REQUEST_CODE_SIGN_IN = 110

internal class GoogleDriveTarget : ExportTarget {

    private var driveResourceClient: DriveResourceClient? = null

    override fun checkConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val signInAccount = GoogleSignIn.getLastSignedInAccount(activity)
        val driveResourceClient = if (signInAccount != null) {
            Drive.getDriveResourceClient(activity, signInAccount)
        } else {
            null
        }
        if (driveResourceClient != null) {
            onConnected(Connection.Success)
        }
    }

    override fun requestConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .build()
        val client = GoogleSignIn.getClient(activity, signInOptions)
        activity.registerActivityResult(REQUEST_CODE_SIGN_IN) { resultCode, _ ->
            if (resultCode == Activity.RESULT_OK) {
                val signInAccount = GoogleSignIn.getLastSignedInAccount(activity)
                if (signInAccount != null) {
                    driveResourceClient = Drive.getDriveResourceClient(activity, signInAccount)
                    onConnected(Connection.Success)
                } else {
                    onConnected(Connection.Failure)
                }
            } else {
                onConnected(Connection.Failure)
            }
        }
        activity.startActivityForResult(client.signInIntent, REQUEST_CODE_SIGN_IN)
    }

    override fun removeConnection(activity: ExportActivity) {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .build()
        val client = GoogleSignIn.getClient(activity, signInOptions)
        client.revokeAccess()
        driveResourceClient = null
    }

    override fun createUploadTask(contentResolver: ContentResolver, trackUri: Uri, callback: Callback): Executable =
            DriveUploadTask(contentResolver, trackUri, callback, checkNotNull(driveResourceClient))
}