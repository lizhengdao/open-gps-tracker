/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.robots

import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import nl.renedegroot.android.gpstracker.ng.features.about.AboutFragment
import nl.renedegroot.android.gpstracker.ng.util.WebViewIdlingResource
import nl.renedegroot.android.gpstracker.v2.R

fun about(activity: AppCompatActivity, block: AboutRobot.() -> AboutRobot) {
    AboutRobot(activity).block()
}

class AboutRobot(private val activity: AppCompatActivity) : Robot<AboutRobot>("AboutScreen") {

    private var resource: WebViewIdlingResource? = null

    fun ok(): AboutRobot {
        Espresso.onView(ViewMatchers.withText(activity.getString(android.R.string.ok)))
                .perform(ViewActions.click())

        return this
    }

    fun start(): AboutRobot {
        waitForIdle()

        val fragment = activity.supportFragmentManager.findFragmentByTag(AboutFragment.TAG) as AboutFragment
        val webview = fragment.requireDialog().findViewById<WebView>(R.id.fragment_about_webview)
        resource = WebViewIdlingResource(webview)
        IdlingRegistry.getInstance().register(resource)

        return this
    }

    fun stop() {
        resource?.let {
            IdlingRegistry.getInstance().unregister(it)
        }
    }
}
