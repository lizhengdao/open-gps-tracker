/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.screenshots

import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import nl.renedegroot.android.gpstracker.ng.Instrumentation
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.robots.Robot
import nl.renedegroot.android.gpstracker.ng.robots.about
import nl.renedegroot.android.gpstracker.ng.robots.export
import nl.renedegroot.android.gpstracker.ng.robots.graph
import nl.renedegroot.android.gpstracker.ng.robots.note
import nl.renedegroot.android.gpstracker.ng.robots.settings
import nl.renedegroot.android.gpstracker.ng.robots.summary
import nl.renedegroot.android.gpstracker.ng.robots.trackList
import nl.renedegroot.android.gpstracker.ng.robots.tracking
import nl.renedegroot.android.gpstracker.ng.util.ClearPreferences
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class TourScreenshots {

    private val activityRule = ActivityTestRule(TrackActivity::class.java)
    private val runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val clearPreferences = ClearPreferences { activityRule.activity }

    @get:Rule
    val ruleChain: RuleChain = RuleChain.emptyRuleChain()
            .around(activityRule)
            .around(runtimePermissionRule)
            .around(clearPreferences)

    companion object {
        @BeforeClass
        @JvmStatic
        fun setupOnce() {
            Instrumentation.active = true
            Robot.resetScreenShots()
        }

        @AfterClass
        @JvmStatic
        fun tearDownOnce() {
            Instrumentation.active = false
        }
    }

    @Test
    fun recordTrack() {
        tracking(activityRule.activity) {
            startRecording()
            sleep(10)
            takeScreenShot()

            pauseRecording()
            takeScreenShot()

            resumeRecording()
            createNote()
        }
        note {
            title("Awesome view")
            picture()
            message("This was after a very cool hike going up the mountain from the hotel.")
            takeScreenShot()

            save()
        }
        tracking(activityRule.activity) {
            openSummary()
        }
        summary {
            sleep(10)
            takeScreenShot()
            pauseTracking()
            back()
        }
        tracking(activityRule.activity) {
            stopRecording()
            takeScreenShot()
        }
    }

    @Test
    fun editTrack() {
        tracking(activityRule.activity) {
            takeScreenShot()
            editTrack()
            takeScreenShot()
            openTrackTypeSpinner()
            takeScreenShot()
            selectWalking()
            ok()
        }
    }

    @Test
    fun trackList() {
        tracking(activityRule.activity) {
            openTrackList().takeScreenShot()
        }
        trackList(activityRule.activity) {
            openSearch()
            checkWalkingTracking()
            takeScreenShot()
            closeSearch()
            openRowContextMenu(0)
            takeScreenShot()
            share()
            takeScreenShot()
            back()
            openRowContextMenu(0)
            edit()
            takeScreenShot()
            cancelEdit()
            openRowContextMenu(0)
            delete()
            takeScreenShot()
            cancelDelete()
        }
    }

    @Test
    fun about() {
        tracking(activityRule.activity) {
            openAbout().takeScreenShot()
        }
        about(activityRule.activity) {
            start()
            ok()
        }
    }

    @Test
    fun configuration() {
        tracking(activityRule.activity) {
            openSettings()
        }
        settings {
            takeScreenShot()
        }
    }

    @Test
    fun export() {
        tracking(activityRule.activity) {
            openTrackList()
        }
        trackList(activityRule.activity) {
            openExport()
        }
        export {
            selectGoogleDrive()
            takeScreenShot()
            selectSDCard()
            grantAccess()
            takeScreenShot()
            export()
            takeScreenShot()
        }
    }

    @Test
    fun graph() {
        tracking(activityRule.activity) {
            openGraph()
        }
        graph {
            selectTime()
            takeScreenShot()
            selectDistance()
            takeScreenShot()
            back()
        }
    }
}
