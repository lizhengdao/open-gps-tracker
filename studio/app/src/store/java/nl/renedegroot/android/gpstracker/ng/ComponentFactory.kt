/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng

import android.app.Application
import android.content.Context
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.dagger.BaseComponent
import nl.renedegroot.android.gpstracker.ng.base.dagger.BaseModule
import nl.renedegroot.android.gpstracker.ng.base.dagger.DaggerBaseComponent
import nl.renedegroot.android.gpstracker.ng.base.dagger.SystemBaseModule
import nl.renedegroot.android.gpstracker.ng.features.dagger.DaggerFeatureComponent
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureComponent
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureModule
import nl.renedegroot.android.gpstracker.ng.features.dagger.VersionInfoModule
import nl.renedegroot.android.gpstracker.v2.BuildConfig
import nl.renedegroot.android.opengpstrack.ng.note.dagger.DaggerNotesComponent
import nl.renedegroot.android.opengpstrack.ng.note.dagger.NotesComponent
import nl.renedegroot.android.opengpstrack.ng.note.dagger.NotesModule
import nl.renedegroot.android.opengpstrack.ng.summary.dagger.DaggerSummaryComponent
import nl.renedegroot.android.opengpstrack.ng.summary.dagger.SummaryComponent
import nl.renedegroot.android.opengpstrack.ng.summary.dagger.SummaryModule
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import kotlin.math.min

class ComponentFactory {
    fun createBaseComponent(application: Application): BaseComponent =
            DaggerBaseComponent.builder()
                    .baseModule(BaseModule(application))
                    .systemBaseModule(SystemBaseModule())
                    .build()

    fun createSummaryComponent(context: Context): SummaryComponent =
            DaggerSummaryComponent.builder()
                    .baseComponent(BaseConfiguration.baseComponent)
                    .summaryModule(SummaryModule(context))
                    .build()

    fun createFeatureComponent(context: Context, summaryManager: SummaryManager): FeatureComponent =
            DaggerFeatureComponent.builder()
                    .baseComponent(BaseConfiguration.baseComponent)
                    .versionInfoModule(VersionInfoModule(version(), gitHash(), buildNumber()))
                    .featureModule(FeatureModule(context, summaryManager))
                    .build()

    fun createNotesComponent(): NotesComponent =
            DaggerNotesComponent.builder()
                    .baseComponent(BaseConfiguration.baseComponent)
                    .notesModule(NotesModule())
                    .build()

    private fun version() = BuildConfig.VERSION_NAME

    private fun buildNumber() = BuildConfig.BUILD_NUMBER.toString()
    
    private fun gitHash() = BuildConfig.GIT_COMMIT.take(min(7, BuildConfig.GIT_COMMIT.length))
}
