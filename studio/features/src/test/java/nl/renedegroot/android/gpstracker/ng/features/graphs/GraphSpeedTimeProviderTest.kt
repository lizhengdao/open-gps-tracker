/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.graphs

import com.nhaarman.mockitokotlin2.any
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.SpeedOverTimeDataProvider
import nl.renedegroot.android.gpstracker.ng.features.util.MockBaseComponentTestRule
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryCalculator
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit

class GraphSpeedTimeProviderTest {

    lateinit var sut: SpeedOverTimeDataProvider

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()!!

    @get:Rule
    var BaseComponentRule = MockBaseComponentTestRule()

    @Mock
    lateinit var calculator: SummaryCalculator

    @Mock
    lateinit var preferences: UnitSettingsPreferences

    @Mock
    lateinit var statisticsFormatter: StatisticsFormatter

    val gpxAmsterdam = listOf(Pair(52.377060, 4.898446), Pair(52.376394, 4.897263), Pair(52.376220, 4.902874), Pair(52.374049, 4.899943))
    val start = 1497243484247L
    val wayPoint1 = Waypoint(0, gpxAmsterdam[0].first, gpxAmsterdam[0].second, start + 180000, 0.0, 0.0)
    val wayPoint2 = Waypoint(0, gpxAmsterdam[0].first, gpxAmsterdam[0].second, wayPoint1.time + 90000, 0.0, 0.0)
    val wayPoint3 = Waypoint(0, gpxAmsterdam[0].first, gpxAmsterdam[0].second, wayPoint2.time + 60000, 0.0, 0.0)
    val wayPoint4 = Waypoint(0, gpxAmsterdam[0].first, gpxAmsterdam[0].second, wayPoint3.time + 90000, 0.0, 0.0)


    @Before
    fun setUp() {
        sut = SpeedOverTimeDataProvider(preferences, false)
        sut.statisticsFormatter = statisticsFormatter
        `when`(calculator.distance(any(), any(), any())).thenReturn(313.37338f)
    }

    @Test
    fun segmentPoints() {
        // Prepare
        val waypoints = listOf(
                Summary.Delta(1L, 1F, 1F, 1L, 0.0),
                Summary.Delta(2L, 2F, 1F, 2L, 0.0))
        // Execute
        val points = sut.calculateSegment(waypoints, start)
        // Assert
        assertThat(points.count(), `is`(2))
    }

}
