/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs

import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.condense
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class GraphDataHelpersTest {

    @Test
    fun `condens empty list`() {
        val list = emptyList<Int>()

        val result = list.condense({ x, y -> x == y }, { it.sum() })

        assertThat(result, `is`(emptyList()))
    }

    @Test
    fun `condens unique list`() {
        val list = listOf(1, 2, 3, 4, 5, 6)

        val result = list.condense({ x, y -> x == y }, { it.sum() })

        assertThat(result, `is`(list))
    }

    @Test
    fun `condens basic list`() {
        val list = listOf(1, 1, 2, 2, 3, 4, 5, 5, 5, 6)

        val result = list.condense({ x, y -> x == y }, { it.sum() })

        assertThat(result, `is`(listOf(2, 4, 3, 4, 15, 6)))
    }

    @Test
    fun `condens repeating list`() {
        val list = listOf(1, 1, 2, 2, 3, 4, 5, 2, 2, 2, 5, 5, 6)

        val result = list.condense({ x, y -> x == y }, { it.sum() })

        assertThat(result, `is`(listOf(2, 4, 3, 4, 5, 6, 10, 6)))
    }
}
