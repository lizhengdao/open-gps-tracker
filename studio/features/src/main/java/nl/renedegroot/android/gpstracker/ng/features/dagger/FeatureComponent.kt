/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.dagger

import dagger.Component
import nl.renedegroot.android.gpstracker.ng.base.dagger.BaseComponent
import nl.renedegroot.android.gpstracker.ng.features.about.AboutModel
import nl.renedegroot.android.gpstracker.ng.features.activityrecognition.ActivityRecognizerLoggingBroadcastReceiver
import nl.renedegroot.android.gpstracker.ng.features.appshortcut.StartLoggingPresenter
import nl.renedegroot.android.gpstracker.ng.features.control.ControlFragment
import nl.renedegroot.android.gpstracker.ng.features.control.ControlPresenter
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.ShareDialog
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.ShareIntentFactory
import nl.renedegroot.android.gpstracker.ng.features.gpximport.GpxImportController
import nl.renedegroot.android.gpstracker.ng.features.gpximport.ImportService
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphsFragment
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphsPresenter
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.AltitudeOverDistanceDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.AltitudeOverTimeDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.SpeedOverDistanceDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.SpeedOverTimeDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.SpeedRangePicker
import nl.renedegroot.android.gpstracker.ng.features.map.TrackMapFragment
import nl.renedegroot.android.gpstracker.ng.features.map.TrackMapPresenter
import nl.renedegroot.android.gpstracker.ng.features.model.TrackSelection
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingFragment
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingNavigation
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingPresenter
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.features.track.TrackNavigator
import nl.renedegroot.android.gpstracker.ng.features.track.TrackPresenter
import nl.renedegroot.android.gpstracker.ng.features.trackdelete.TrackDeletePresenter
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackEditPresenter
import nl.renedegroot.android.gpstracker.ng.features.tracklist.ImportNotification
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListActivity
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListFragment
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListNavigation
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListNavigator
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListPresenter
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListViewAdapter
import nl.renedegroot.android.gpstracker.ng.features.wear.PhoneMessageListenerService
import nl.renedegroot.android.gpstracker.ng.features.wear.StatisticsCollector
import nl.renedegroot.android.gpstracker.ng.features.wear.WearLoggingService
import nl.renedegroot.android.gpstracker.ng.features.wear.WearLoggingStateBroadcastReceiver
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter

@FeatureScope
@Component(modules = [FeatureModule::class, VersionInfoModule::class],
        dependencies = [BaseComponent::class])
interface FeatureComponent {

    fun statisticsFormatter(): StatisticsFormatter

    fun trackSelection(): TrackSelection

    fun inject(injectable: TrackDeletePresenter)
    fun inject(injectable: TrackEditPresenter)
    fun inject(injectable: GraphsPresenter)
    fun inject(injectable: TrackPresenter)
    fun inject(injectable: TrackMapPresenter)
    fun inject(injectable: TrackListPresenter)
    fun inject(injectable: RecordingPresenter)

    fun inject(trackNavigator: TrackNavigator)
    fun inject(gpxImportController: GpxImportController)
    fun inject(importService: ImportService)
    fun inject(aboutModel: AboutModel)
    fun inject(controlPresenter: ControlPresenter)
    fun inject(recordingNavigation: RecordingNavigation)
    fun inject(importNotification: ImportNotification)
    fun inject(trackListNavigation: TrackListNavigation)
    fun inject(trackListViewAdapter: TrackListViewAdapter)
    fun inject(wearLoggingService: WearLoggingService)
    fun inject(phoneMessageListenerService: PhoneMessageListenerService)
    fun inject(statisticsCollector: StatisticsCollector)
    fun inject(speedOverTimeProvider: SpeedOverTimeDataProvider)
    fun inject(speedOverDistanceDataProvider: SpeedOverDistanceDataProvider)
    fun inject(controlFragment: ControlFragment)
    fun inject(trackListActivity: TrackListActivity)
    fun inject(trackActivity: TrackActivity)
    fun inject(receiver: WearLoggingStateBroadcastReceiver)
    fun inject(receiver: ActivityRecognizerLoggingBroadcastReceiver)
    fun inject(recordingFragment: RecordingFragment)
    fun inject(graphsFragment: GraphsFragment)
    fun inject(trackMapFragment: TrackMapFragment)
    fun inject(trackListFragment: TrackListFragment)
    fun inject(speedRangePicker: SpeedRangePicker)
    fun inject(shareIntentFactory: ShareIntentFactory)
    fun inject(altitudeOverDistanceDataProvider: AltitudeOverDistanceDataProvider)
    fun inject(altitudeOverTimeDataProvider: AltitudeOverTimeDataProvider)
    fun inject(startLoggingPresenter: StartLoggingPresenter)
    fun inject(trackListNavigator: TrackListNavigator)
    fun inject(shareDialog: ShareDialog)
}
