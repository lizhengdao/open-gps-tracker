/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.map.util.onMap
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionRequester
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentMapBinding
import javax.inject.Inject

class TrackMapFragment : Fragment() {

    @Inject
    lateinit var permissionRequester: PermissionRequester
    private lateinit var presenter: TrackMapPresenter
    private var binding: FragmentMapBinding? = null
    private val mapView
        get() = binding!!.fragmentMapMapview

    private val optionMenuObserver: Observable.OnPropertyChangedCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            activity?.invalidateOptionsMenu()
        }
    }

    private val wakelockObservable: Observable.OnPropertyChangedCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            val wakelock = presenter.viewModel.isLocked.get()
            if (wakelock) {
                activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else {
                activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        FeatureConfiguration.featureComponent.inject(this)

        presenter = ViewModelProvider(this).get(TrackMapPresenter::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentMapBinding>(inflater, R.layout.fragment_map, container, false)
        binding.fragmentMapMapview.onCreate(savedInstanceState)
        binding.viewModel = presenter.viewModel
        binding.presenter = presenter
        presenter.viewModel.showSatellite.addOnPropertyChangedCallback(optionMenuObserver)
        presenter.viewModel.willLock.addOnPropertyChangedCallback(optionMenuObserver)
        presenter.viewModel.isLocked.addOnPropertyChangedCallback(wakelockObservable)
        presenter.navigation.observe(viewLifecycleOwner, MapNavigator(parentFragmentManager))
        this.binding = binding

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        presenter.onShowMapView(mapView)
        permissionRequester.start(this) {
            presenter.start()
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        permissionRequester.stop(this) {
            presenter.stop()
        }
        mapView.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mapView.apply {
            onMap { it.isMyLocationEnabled = false }
            onDestroy()
        }
        binding = null
        presenter.viewModel.showSatellite.removeOnPropertyChangedCallback(optionMenuObserver)
        presenter.viewModel.willLock.removeOnPropertyChangedCallback(optionMenuObserver)
        presenter.viewModel.isLocked.removeOnPropertyChangedCallback(wakelockObservable)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.map, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        menu.findItem(R.id.action_satellite).isChecked = presenter.viewModel.showSatellite.get()
        menu.findItem(R.id.action_lock).isChecked = presenter.viewModel.willLock.get()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.action_satellite -> {
                presenter.onSatelliteSelected()
                true
            }
            item.itemId == R.id.action_lock -> {
                presenter.onScreenLockSelected()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding?.fragmentMapMapview?.onLowMemory()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionRequester.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
    }
}
