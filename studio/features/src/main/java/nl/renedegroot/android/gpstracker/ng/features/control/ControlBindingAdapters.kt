/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.control

import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_LOGGING
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_PAUSED
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_STOPPED
import nl.renedegroot.android.opengpstrack.ng.features.R

@BindingAdapter("state")
fun setState(container: ViewGroup, state: Int) {
    val left = container.getChildAt(0) as FloatingActionButton
    val right = container.getChildAt(1) as FloatingActionButton
    cancelAnimations(left, right)
    if (state == STATE_STOPPED) {
        right.setImageResource(R.drawable.ic_navigation_black_24dp)
        right.contentDescription = container.context.getString(R.string.control_record)
        showOnlyRightButton(left, right)
    } else if (state == STATE_LOGGING) {
        left.setImageResource(R.drawable.ic_stop_black_24dp)
        left.contentDescription = container.context.getString(R.string.control_stop)
        right.setImageResource(R.drawable.ic_pause_black_24dp)
        right.contentDescription = container.context.getString(R.string.control_pause)
        showAllButtons(left, right)
    } else if (state == STATE_PAUSED) {
        right.setImageResource(R.drawable.ic_navigation_black_24dp)
        right.contentDescription = container.context.getString(R.string.control_resume)
        left.setImageResource(R.drawable.ic_stop_black_24dp)
        left.contentDescription = container.context.getString(R.string.control_stop)
        showAllButtons(left, right)
    } else {
        // state == STATE_UNKNOWN and illegal states
        showNoButtons(left, right)
    }
}

private fun showNoButtons(left: FloatingActionButton, right: FloatingActionButton) {
    left.animate()
            .alpha(0F)
    right.animate()
            .alpha(0F)
}

private fun showAllButtons(left: FloatingActionButton, right: FloatingActionButton) {
    left.animate()
            .alpha(1F)
    right.animate()
            .alpha(1F)
}

private fun showOnlyRightButton(left: FloatingActionButton, right: FloatingActionButton) {
    left.animate()
            .alpha(0F)
    right.animate()
            .alpha(1F)
}

private fun cancelAnimations(left: FloatingActionButton, right: FloatingActionButton) {
    left.animate().cancel()
    right.animate().cancel()
}

private val View.visible: Boolean
    get() {
        return this.visibility == View.VISIBLE
    }

private val View.invisible: Boolean
    get() {
        return this.visibility != View.VISIBLE
    }
