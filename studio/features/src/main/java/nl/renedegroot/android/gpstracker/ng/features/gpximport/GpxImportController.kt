/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.content.Context
import android.net.Uri
import android.os.Build
import android.provider.DocumentsContract
import android.provider.DocumentsContract.Document.COLUMN_DISPLAY_NAME
import android.provider.DocumentsContract.Document.COLUMN_DOCUMENT_ID
import android.provider.DocumentsContract.Document.COLUMN_MIME_TYPE
import androidx.annotation.RequiresApi
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.features.tracklist.ImportNotification
import nl.renedegroot.android.gpstracker.service.util.saveTrackType
import nl.renedegroot.android.gpstracker.utils.contentprovider.countResult
import nl.renedegroot.android.gpstracker.utils.contentprovider.getString
import nl.renedegroot.android.gpstracker.utils.contentprovider.map
import nl.renedegroot.opengpstracker.exporter.GpxCreator.MIME_TYPE_GPX
import org.xmlpull.v1.XmlPullParserException
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class GpxImportController(private val context: Context) {

    @Inject
    lateinit var parser: GpxParserFactory
    @Inject
    lateinit var notification: ImportNotification

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun import(uri: Uri, trackType: String) {
        notification.didStartImport()
        notification.onProgress(0, 1)
        importTrack(uri, trackType)
        notification.onProgress(1, 1)
        notification.didCompleteImport()
    }

    private fun importTrack(uri: Uri, trackType: String) {
        val defaultName = extractName(uri)
        try {
            val openInputStream = context.contentResolver.openInputStream(uri)
            openInputStream?.use {
                parser.parseTrack(openInputStream, defaultName).forEach {
                    it.saveTrackType(TrackTypeDescriptions.trackTypeForContentType(trackType))
                }
            }
        } catch (e: XmlPullParserException) {
            notification.didFailImport(retry = false)
        } catch (e: IOException) {
            notification.didFailImport(retry = true)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun importDirectory(uri: Uri, trackType: String) {
        notification.didStartImport()
        val childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri))
        val projection = listOf(COLUMN_DOCUMENT_ID, COLUMN_MIME_TYPE, COLUMN_DISPLAY_NAME)
        val count = childrenUri.countResult(context.contentResolver)
        var progress = 0
        notification.onProgress(progress, count)
        childrenUri.map(context.contentResolver, projection = projection) {
            val id = it.getString(COLUMN_DOCUMENT_ID)
            val mimeType = it.getString(COLUMN_MIME_TYPE)
            val name = it.getString(COLUMN_DISPLAY_NAME)
            if (mimeType == MIME_TYPE_GPX || name?.endsWith(".gpx", true) == true) {
                importTrack(DocumentsContract.buildDocumentUriUsingTree(uri, id), trackType)
            } else {
                Timber.e("Will not import file $name")
            }
            progress++
            notification.onProgress(progress, count)
        }
        notification.didCompleteImport()
    }

    private fun extractName(uri: Uri): String {
        val startIndex = checkNotNull(uri.lastPathSegment).indexOfLast { it == '/' }

        return if (startIndex != -1) {
            checkNotNull(uri.lastPathSegment).substring(startIndex + 1).removeSuffix(".gpx")
        } else {
            checkNotNull(uri.lastPathSegment).removeSuffix(".gpx")
        }
    }
}

