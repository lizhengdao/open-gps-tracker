/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.track

import android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import nl.renedegroot.android.gpstracker.ng.features.about.AboutFragment
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphsFragment
import nl.renedegroot.android.gpstracker.ng.features.graphs.startGraphsActivity
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackEditDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListFragment
import nl.renedegroot.android.gpstracker.ng.features.tracklist.startTrackListActivity
import nl.renedegroot.android.gpstracker.ng.settings.startSettingsActivity
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.opengpstrack.ng.features.R
import timber.log.Timber
import javax.inject.Inject

class TrackNavigation @Inject constructor() {

    internal val step = MutableLiveData<Consumable<TrackNavigationStep>>()

    fun showAboutDialog() {
        step.postValue(Consumable(TrackNavigationStep.AboutDialog))
    }

    fun showTrackEditDialog(trackUri: Uri) {
        step.postValue(Consumable(TrackNavigationStep.TrackEditDialog(trackUri)))
    }

    fun showGraphs() {
        step.postValue(Consumable(TrackNavigationStep.Graphs))
    }

    fun showTrackSelection() {
        step.postValue(Consumable(TrackNavigationStep.TrackSelection))
    }

    fun showSettings() {
        step.postValue(Consumable(TrackNavigationStep.Settings))
    }
}

internal sealed class TrackNavigationStep {
    object AboutDialog : TrackNavigationStep()
    data class TrackEditDialog(val trackUri: Uri) : TrackNavigationStep()
    object Graphs : TrackNavigationStep()
    object TrackSelection : TrackNavigationStep()
    object Settings : TrackNavigationStep()
}

private const val TAG_DIALOG = "DIALOG"
private const val TRANSACTION_TRACKS = "FRAGMENT_TRANSACTION_TRACKS"
private const val TRANSACTION_GRAPHS = "FRAGMENT_TRANSACTION_GRAPGS"

class TrackNavigator(val activity: FragmentActivity) {

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun observe(navigation: TrackNavigation) {
        navigation.step.observe(activity, Observer { consumable ->
            consumable.consume { navigate(it) }
        })
    }

    private fun navigate(step: TrackNavigationStep) =
            when (step) {
                TrackNavigationStep.AboutDialog -> showAboutDialog()
                is TrackNavigationStep.TrackEditDialog -> showTrackEditDialog(step.trackUri)
                TrackNavigationStep.Graphs -> showGraphs()
                TrackNavigationStep.TrackSelection -> showTrackSelection()
                TrackNavigationStep.Settings -> showSettings()
            }

    private fun showAboutDialog() {
        AboutFragment().show(activity.supportFragmentManager, AboutFragment.TAG)
    }

    private fun showTrackEditDialog(trackUri: Uri) {
        TrackEditDialogFragment.newInstance(trackUri).show(activity.supportFragmentManager, TAG_DIALOG)
    }

    private fun showGraphs() {
        if (hasLeftContainer()) {
            toggleContainerFragment(GraphsFragment.newInstance(), TRANSACTION_GRAPHS)
        } else {
            startGraphsActivity(activity)
        }
    }

    private fun showTrackSelection() {
        if (hasLeftContainer()) {
            toggleContainerFragment(TrackListFragment.newInstance(), TRANSACTION_TRACKS)
        } else {
            startTrackListActivity(activity)
        }
    }

    private fun showSettings() {
        startSettingsActivity(activity)
    }

    private fun hasLeftContainer(): Boolean {
        val leftContainer = activity.findViewById<View>(R.id.track_leftcontainer)
        return leftContainer != null && leftContainer is ViewGroup
    }


    private fun toggleContainerFragment(goal: androidx.fragment.app.Fragment, tag: String) {
        val fragment = activity.supportFragmentManager.findFragmentById(R.id.track_leftcontainer)
        if (fragment != null) {
            if (fragment is TrackListFragment) {
                activity.supportFragmentManager.popBackStack(TRANSACTION_TRACKS, POP_BACK_STACK_INCLUSIVE)
            } else if (fragment is GraphsFragment) {
                activity.supportFragmentManager.popBackStack(TRANSACTION_GRAPHS, POP_BACK_STACK_INCLUSIVE)
            }
        }
        if (fragment == null || fragment.javaClass != goal.javaClass) {
            replaceFragmentInLeftContainer(goal, tag)
        }
    }

    private fun replaceFragmentInLeftContainer(goal: androidx.fragment.app.Fragment, tag: String) {
        try {
            activity.supportFragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left,
                            R.anim.enter_from_left, R.anim.exit_to_left)
                    .addToBackStack(tag)
                    .replace(R.id.track_leftcontainer, goal)
                    .commit()
        } catch (e: Exception) {
            Timber.e(e, "Transaction to add Fragment failed")
        }
    }
}
