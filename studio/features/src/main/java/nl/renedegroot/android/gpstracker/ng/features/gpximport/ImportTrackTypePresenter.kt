/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport

import androidx.lifecycle.ViewModel
import android.view.View
import android.widget.AdapterView
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions

class ImportTrackTypePresenter : ViewModel() {

    val model = ImportTrackTypeModel()

    var resultLambda: (String) -> Unit = {}

    val onItemSelectedListener: AdapterView.OnItemSelectedListener by lazy {
        object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                model.selectedPosition.set(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                model.selectedPosition.set(AdapterView.INVALID_POSITION)
            }
        }
    }

    fun ok() {
        model.dismiss.set(true)
        val i = model.selectedPosition.get()
        val trackType = if (i == AdapterView.INVALID_POSITION) {
            TrackTypeDescriptions.allTrackTypes[0].contentValue
        } else {
            TrackTypeDescriptions.allTrackTypes[i].contentValue
        }
        resultLambda(trackType)
    }

    fun cancel() {
        model.dismiss.set(true)
    }
}
