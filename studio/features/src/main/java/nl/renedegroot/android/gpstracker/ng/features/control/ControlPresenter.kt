/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.control

import android.content.ContentResolver
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.base.dagger.DiskIO
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionRequester
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.model.Preferences
import nl.renedegroot.android.gpstracker.ng.features.model.TrackSelection
import nl.renedegroot.android.gpstracker.ng.features.model.not
import nl.renedegroot.android.gpstracker.ng.features.trackedit.NameGenerator
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractPresenter
import nl.renedegroot.android.gpstracker.service.util.LoggingStateController
import nl.renedegroot.android.gpstracker.service.util.LoggingStateListener
import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.*
import nl.renedegroot.android.gpstracker.service.util.trackUri
import nl.renedegroot.android.gpstracker.service.util.updateName
import nl.renedegroot.android.gpstracker.service.util.waypointsUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.runQuery
import nl.renedegroot.android.gpstracker.utils.preference.LoggingSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.preference.activeInterval
import nl.renedegroot.android.opengpstrack.ng.features.R
import java.util.*
import java.util.concurrent.Executor
import javax.inject.Inject

class ControlPresenter : AbstractPresenter(), LoggingStateListener {

    internal val viewModel = ControlViewModel()
    private val handler = Handler(Looper.getMainLooper())
    private val enableRunnable = { enableButtons() }
    @Inject
    lateinit var nameGenerator: NameGenerator
    @Inject
    @field:DiskIO
    lateinit var executor: Executor
    @Inject
    lateinit var loggingStateController: LoggingStateController
    @Inject
    lateinit var serviceCommander: ServiceCommander
    @Inject
    lateinit var trackSelection: TrackSelection
    @Inject
    lateinit var contentResolver: ContentResolver
    @Inject
    lateinit var navigation: ControlNavigation
    @Inject
    lateinit var loggingSettingsPreferences: LoggingSettingsPreferences
    @Inject
    lateinit var permissionRequester: PermissionRequester
    @Inject
    lateinit var preferences: Preferences

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override suspend fun onStart() {
        super.onStart()
        loggingStateController.connect(this)
    }

    override suspend fun onStop() {
        super.onStop()
        loggingStateController.disconnect()
    }

    @WorkerThread
    override suspend fun onChange() {
        val state = loggingStateController.loggingState
        viewModel.state.set(state)
        if (state != ServiceConstants.STATE_UNKNOWN) {
            enableButtons()
        }

        loggingStateController.trackUri?.let {
            if (state == STATE_LOGGING) {
                trackSelection.selection.postValue(it)
                executor.execute {
                    if (serviceCommander.hasForInitialName(it)) {
                        val generatedName = nameGenerator.generateName(Calendar.getInstance())
                        it.updateName(generatedName)
                    }
                }
            }
        }
    }

    //region Service connection

    override fun didStartLogging(trackUri: Uri) {
        markDirty()
    }

    override fun didPauseLogging(trackUri: Uri) {
        markDirty()
    }

    override fun didStopLogging() {
        markDirty()
    }

    //endregion

    //region View callback

    fun onClickLeft() {
        disableUntilChange(200)
        if (viewModel.state.get() == STATE_LOGGING) {
            confirmStopLogging()
        } else if (viewModel.state.get() == STATE_PAUSED) {
            confirmStopLogging()
        }
    }

    fun onClickRight() {
        disableUntilChange(200)
        when {
            viewModel.state.get() == STATE_STOPPED -> startLogging()
            viewModel.state.get() == STATE_LOGGING -> pauseLogging()
            viewModel.state.get() == STATE_PAUSED -> resumeLogging()
        }
    }

    //endregion

    private fun disableUntilChange(timeout: Long) {
        viewModel.enabled.set(false)
        handler.postDelayed(enableRunnable, timeout)
    }

    private fun enableButtons() {
        handler.removeCallbacks { enableRunnable }
        viewModel.enabled.set(true)
    }

    private fun startLogging() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            navigation.checkBackgroundPermission()
        } else {
            startLoggingWithBackgroundPermission(true)
        }
    }

    fun startLoggingWithBackgroundPermission(granted: Boolean) {
        val interval = loggingSettingsPreferences.activeInterval
        if (granted){
            preferences.wakelockScreen.value = false
            serviceCommander.startGPSLogging(R.string.initial_track_name, interval)
        } else {
            preferences.wakelockScreen.value = true
            serviceCommander.startGPSLogging(R.string.initial_track_name, interval)
        }
    }

    private fun confirmStopLogging() {
        navigation.showStopLoggingConfirmation()
    }

    fun stopLogging() {
        serviceCommander.stopGPSLogging()
        deleteEmptyTrack()
    }

    private fun pauseLogging() {
        serviceCommander.pauseGPSLogging()
    }

    private fun resumeLogging() {
        serviceCommander.resumeGPSLogging()
    }

    private fun deleteEmptyTrack() {
        val trackId = loggingStateController.trackUri?.lastPathSegment?.toLongOrNull() ?: -1L
        if (trackId <= 0) {
            return
        }

        val waypointsUri = waypointsUri(trackId)
        val firstWaypointId = waypointsUri.runQuery(contentResolver) { it.getLong(0) } ?: -1L
        if (firstWaypointId == -1L) {
            contentResolver.delete(trackUri(trackId), null, null)
        }
    }
}
