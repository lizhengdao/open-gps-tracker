/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.net.Uri
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.annotation.StringRes
import androidx.cardview.widget.CardView
import androidx.core.view.forEach
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import nl.renedegroot.android.gpstracker.ng.base.common.postMainThread
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.allTrackTypes
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.TrackType
import nl.renedegroot.android.opengpstrack.ng.features.R

@BindingAdapter("selected")
fun RecyclerView.setTracks(track: Uri?) {
    val viewAdapter: TrackListViewAdapter
    if (adapter is TrackListViewAdapter) {
        viewAdapter = adapter as TrackListViewAdapter
        viewAdapter.selection = track
    } else {
        viewAdapter = TrackListViewAdapter(context)
        viewAdapter.selection = track
        adapter = viewAdapter
    }
}

@BindingAdapter("tracks", "tracksListener")
fun RecyclerView.setTracks(tracks: List<Uri>?, listener: TrackListAdapterListener?) {
    val viewAdapter: TrackListViewAdapter
    if (adapter is TrackListViewAdapter) {
        viewAdapter = adapter as TrackListViewAdapter
        viewAdapter.updateTracks(tracks ?: emptyList())
    } else {
        viewAdapter = TrackListViewAdapter(context)
        viewAdapter.updateTracks(tracks ?: emptyList())
        adapter = viewAdapter
    }
    viewAdapter.listener = listener
}


@BindingAdapter("editMode")
fun CardView.setEditMode(editMode: Boolean) {
    val share = findViewById<View>(R.id.row_track_share)
    val delete = findViewById<View>(R.id.row_track_delete)
    val edit = findViewById<View>(R.id.row_track_edit)
    if (editMode) {
        if (share.visibility != VISIBLE) {
            share.alpha = 0F
            edit.alpha = 0F
            delete.alpha = 0F
        }
        share.visibility = VISIBLE
        edit.visibility = VISIBLE
        delete.visibility = VISIBLE
        share.animate().alpha(1.0F)
        edit.animate().alpha(1.0F)
        delete.animate().alpha(1.0F)
    } else if (share.visibility == VISIBLE) {
        share.animate().alpha(0.0F)
        edit.animate().alpha(0.0F)
        delete.animate().alpha(0.0F).withEndAction {
            share.visibility = GONE
            edit.visibility = GONE
            delete.visibility = GONE
        }
    }
}

@BindingAdapter("focusPosition")
fun RecyclerView.setFocusPosition(position: Int?) {
    if (position != null && position > 0) {
        postMainThread { layoutManager?.scrollToPosition(position) }
    }
}

@BindingAdapter("trackType")
fun Chip.setTrackType(@StringRes resId: Int) {
    setText(resId)
    tag = allTrackTypes.find { it.stringId == resId }
}

@BindingAdapter("selectedChips")
fun ChipGroup.setSelectedChips(selectedChips: List<TrackType>) {
    forEach { view ->
        (view as? Chip)?.let { chip ->
            chip.isChecked = selectedChips.contains(chip.tag)
        }
    }
}