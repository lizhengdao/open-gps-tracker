/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.utils.activityresult.FragmentResultLambda
import nl.renedegroot.android.gpstracker.utils.observe
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentImportTracktypeDialogBinding

class ImportTrackTypeDialogFragment : DialogFragment() {

    private lateinit var presenter: ImportTrackTypePresenter

    fun show(manager: androidx.fragment.app.FragmentManager, tag: String, resultLambda: (String) -> Unit) {
        val lambdaHolder = FragmentResultLambda<String>()
        lambdaHolder.resultLambda = resultLambda
        manager.beginTransaction().add(lambdaHolder, TAG_LAMBDA_FRAGMENT).commit()
        setTargetFragment(lambdaHolder, 324)

        super.show(manager, tag)
    }

    override fun dismiss() {
        parentFragmentManager.let { fragmentManager ->
            val fragment = fragmentManager.findFragmentByTag(TAG_LAMBDA_FRAGMENT)
            if (fragment != null) {
                fragmentManager.beginTransaction()
                        .remove(fragment)
                        .commit()
            }
        }
        super.dismiss()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ViewModelProvider(this).get(ImportTrackTypePresenter::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentImportTracktypeDialogBinding>(inflater, R.layout.fragment_import_tracktype_dialog, container, false)
        val importTrackTypePresenter = ViewModelProvider(this).get(ImportTrackTypePresenter::class.java)
        binding.presenter = importTrackTypePresenter
        binding.model = presenter.model
        presenter.model.dismiss.observe { sender ->
            if (sender is ObservableBoolean && sender.get()) {
                dismiss()
            }
        }
        binding.fragmentImporttracktypeSpinner.onItemSelectedListener = importTrackTypePresenter.onItemSelectedListener
        if (targetFragment is FragmentResultLambda<*>) {
            importTrackTypePresenter.resultLambda = (targetFragment as FragmentResultLambda<String>).resultLambda
        }
        presenter = importTrackTypePresenter

        return binding.root
    }

    companion object {

        private const val TAG_LAMBDA_FRAGMENT = "TAG_LAMBDA_FRAGMENT"
    }
}
