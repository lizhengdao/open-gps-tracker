/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

import org.jetbrains.annotations.NotNull;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class GpxParserFactory {

    private final ContentResolver contentResolver;

    public GpxParserFactory(Context context) {
        this.contentResolver = context.getContentResolver();
    }

    /**
     * Read a stream containing GPX XML into the OGT content provider
     *
     * @param inputStream opened stream the read from, will be closed after this call
     * @param defaultName
     * @return content provider uri
     */
    public List<Uri> parseTrack(@NotNull InputStream inputStream, String defaultName) throws XmlPullParserException, IOException {
        return new GpxElementsParser(contentResolver, defaultName).parse(inputStream);
    }
}
