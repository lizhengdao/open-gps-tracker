/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.net.Uri
import androidx.lifecycle.Observer
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.content.ContentController
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.ShareIntentFactory
import nl.renedegroot.android.gpstracker.ng.features.model.TrackSearch
import nl.renedegroot.android.gpstracker.ng.features.model.TrackSelection
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Tracks._ID
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Tracks.CREATION_TIME
import nl.renedegroot.android.gpstracker.service.util.readTrackType
import nl.renedegroot.android.gpstracker.service.util.trackUri
import nl.renedegroot.android.gpstracker.service.util.tracksUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.getLong
import nl.renedegroot.android.gpstracker.utils.contentprovider.map
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractPresenter
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import javax.inject.Inject

class TrackListPresenter : AbstractPresenter(), ContentController.Listener, TrackListAdapterListener {

    @Inject
    lateinit var contentController: ContentController

    @Inject
    lateinit var trackSelection: TrackSelection

    @Inject
    lateinit var trackSearch: TrackSearch

    @Inject
    lateinit var summaryManager: SummaryManager

    @Inject
    lateinit var shareIntentFactory: ShareIntentFactory

    @Inject
    lateinit var notification: ImportNotification

    @Inject
    lateinit var navigation: TrackListNavigation

    val viewState: TrackListViewState = TrackListViewState()
    private val selectionObserver = Observer<Uri> { trackUri -> onTrackSelected(trackUri) }
    private val searchQueryObserver = Observer<String> { _ -> onSearchQuery() }

    private val selection: Pair<String, List<String>>?
        get() {
            val argument = trackSearch.query.value.asArgument()
            return if (argument.isBlank()) {
                null
            } else {
                Pair("name LIKE ?", listOf(argument))
            }
        }

    init {
        FeatureConfiguration.featureComponent.inject(this)
        trackSelection.selection.observeForever(selectionObserver)
        trackSearch.query.observeForever(searchQueryObserver)
        contentController.registerObserver(this, tracksUri())
    }

    override suspend fun onStart() {
        super.onStart()
        summaryManager.start()
    }

    override suspend fun onChange() {
        val filters = checkNotNull(viewState.activeFilters.get())
        val trackList = tracksUri().map(
                BaseConfiguration.baseComponent.contentResolver(),
                selection,
                projection = listOf(_ID, CREATION_TIME),
                sortOrder = "$CREATION_TIME DESC, $_ID DESC"
        ) {
            val id = it.getLong(_ID)!!
            trackUri(id)
        }.filter { trackUri ->
            filters.isEmpty() || filters.contains(trackUri.readTrackType())
        }
        viewState.selectedTrack.set(trackSelection.selection.value)
        viewState.tracks.set(trackList)
        trackSelection.selection.value?.let { scrollToTrack(it) }
    }

    override suspend fun onStop() {
        summaryManager.stop()
        super.onStop()
    }

    public override fun onCleared() {
        trackSelection.selection.removeObserver(selectionObserver)
        trackSearch.query.removeObserver(searchQueryObserver)
        contentController.unregisterObserver()
        notification.dismissCompletedImport()
        super.onCleared()
    }

    /* Content watching */

    override fun onChangeUriContent(contentUri: Uri, changesUri: Uri) {
        markDirty()
    }

    /* Content retrieval */

    private fun scrollToTrack(trackUri: Uri) {
        val position = viewState.tracks.get()?.indexOf(trackUri)
        position?.let {
            viewState.focusPosition.set(position)
        }
    }

    //region View (adapter) callbacks

    override fun didSelectTrack(track: Uri, name: String) {
        trackSelection.selection.value = track
        navigation.finishTrackSelection()
    }

    override fun didEditTrack(track: Uri) {
        navigation.showTrackEditDialog(track)
    }

    override fun didDeleteTrack(track: Uri) {
        navigation.showTrackDeleteDialog(track)
    }

    override fun didSelectExportToDirectory() {
        navigation.startExporter()
    }

    override fun didSelectImportTrack() {
        navigation.startGpxFileSelection()
    }

    override fun didSelectImportFromDirectory() {
        navigation.startGpxDirectorySelection()
    }

    override fun didShareTrack(track: Uri) {
        navigation.showSharePicker(track)
    }

    fun onShareGpxFile(track: Uri) {
        navigation.gpxShare(track)
    }

    fun onShareSocial(track: Uri) {
        navigation.socialShare(track)
    }

    fun onSearchOpened() {
        viewState.showFilter.set(true)
    }

    internal fun onSearchClosed() {
        viewState.showFilter.set(false)
        viewState.activeFilters.set(emptyList())
        trackSearch.query.value = null
    }

    //endregion

    // region Filtering

    fun didClickChip(trackType: Any) {
        val activeSelections = checkNotNull(viewState.activeFilters.get())
        val newSelections = if (activeSelections.contains(trackType)) {
            activeSelections.minus(trackType as TrackTypeDescriptions.TrackType)
        } else {
            activeSelections.plus(trackType as TrackTypeDescriptions.TrackType)
        }
        viewState.activeFilters.set(newSelections)
        markDirty()
    }

    // endregion

    //region Track selection listening

    private fun onTrackSelected(trackUri: Uri?) {
        viewState.selectedTrack.set(trackUri)
    }

    private fun onSearchQuery() {
        markDirty()
    }

    //endregion

    private fun String?.asArgument(): String =
            if (this == null) {
                ""
            } else {
                val filtered = this.filter { it != '%' }
                if (filtered.isNotBlank()) {
                    "%$filtered%"
                } else {
                    ""
                }

            }
}

