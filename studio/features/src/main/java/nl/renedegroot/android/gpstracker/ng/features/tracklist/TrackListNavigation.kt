/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.content.Intent
import android.content.Intent.ACTION_OPEN_DOCUMENT
import android.content.Intent.ACTION_OPEN_DOCUMENT_TREE
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.KEY_META_FIELD_TRACK_TYPE
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.MIME_TYPE_ANY
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.ShareIntentFactory
import nl.renedegroot.android.gpstracker.ng.features.gpximport.ImportService
import nl.renedegroot.android.gpstracker.ng.features.gpximport.ImportTrackTypeDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.features.trackdelete.TrackDeleteDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackEditDialogFragment
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.gpstracker.utils.VersionHelper
import nl.renedegroot.android.gpstracker.utils.activityresult.ActivityResultLambda
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import javax.inject.Inject

private const val TAG_DIALOG = "DIALOG"

class TrackListNavigation @Inject constructor() {

    val navigation = MutableLiveData<Consumable<Navigation>>()

    fun finishTrackSelection() {
        navigation.postValue(Consumable(Navigation.Finish))
    }

    fun showTrackEditDialog(track: Uri) {
        navigation.postValue(Consumable(Navigation.Edit(track)))
    }

    fun showTrackDeleteDialog(track: Uri) {
        navigation.postValue(Consumable(Navigation.Delete(track)))
    }

    fun showSharePicker(track: Uri) {
        navigation.postValue(Consumable(Navigation.SharePicker(track)))
    }

    fun startGpxFileSelection() {
        navigation.postValue(Consumable(Navigation.GpxFileSelection))
    }

    fun startGpxDirectorySelection() {
        navigation.postValue(Consumable(Navigation.GpxDirectorySelection))
    }

    fun startExporter() {
        navigation.postValue(Consumable(Navigation.Exporter))
    }

    fun socialShare(track: Uri) {
        navigation.postValue(Consumable(Navigation.SocialShare(track)))
    }

    fun gpxShare(track: Uri) {
        navigation.postValue(Consumable(Navigation.GpxShare(track)))
    }
}

sealed class Navigation {
    object Finish : Navigation()
    data class Edit(val track: Uri) : Navigation()
    data class Delete(val track: Uri) : Navigation()
    data class SharePicker(val intent: Uri) : Navigation()
    object GpxFileSelection : Navigation()
    object Dismiss : Navigation()
    object GpxDirectorySelection : Navigation()
    object Exporter : Navigation()
    data class SocialShare(val track: Uri) : Navigation()
    data class GpxShare(val track: Uri) : Navigation()
}

class TrackListNavigator(
        private val activity: FragmentActivity,
        private val childFragmentManager: FragmentManager
) {

    @Inject
    lateinit var versionHelper: VersionHelper

    @Inject
    lateinit var shareIntentFactory: ShareIntentFactory

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun observe(owner: LifecycleOwner, navigation: TrackListNavigation) {
        navigation.navigation.observe(owner, { consumable ->
            consumable.consume { destination ->
                navigate(destination)
            }
        })
    }

    private fun navigate(destination: Navigation) =
            when (destination) {
                Navigation.Finish -> finishTrackSelection()
                is Navigation.Edit -> showTrackEditDialog(destination.track)
                is Navigation.Delete -> showTrackDeleteDialog(destination.track)
                is Navigation.SharePicker -> showSharePicker(destination.intent)
                Navigation.GpxFileSelection -> startGpxFileSelection { intent: Intent ->
                    val uri = intent.data
                    uri?.let {
                        val trackType = intent.getStringExtra(KEY_META_FIELD_TRACK_TYPE)
                                ?: TrackTypeDescriptions.VALUE_TYPE_DEFAULT
                        ImportService.importFile(uri, trackType)
                    }
                }
                Navigation.GpxDirectorySelection -> startGpxDirectorySelection { intent: Intent ->
                    val uri = intent.data
                    uri?.let {
                        val trackType = intent.getStringExtra(KEY_META_FIELD_TRACK_TYPE)
                                ?: TrackTypeDescriptions.VALUE_TYPE_DEFAULT
                        ImportService.importDirectory(uri, trackType)
                    }
                }
                Navigation.Exporter -> startExporter()
                Navigation.Dismiss -> dismissDialog()
                is Navigation.SocialShare -> socialShare(destination.track)
                is Navigation.GpxShare -> gpxShare(destination.track)
            }

    private fun socialShare(track: Uri) {
        dismissDialog()
        shareIntentFactory.createSocialShareIntent(activity, track)
    }

    private fun gpxShare(track: Uri) {
        dismissDialog()
        shareIntentFactory.createGpxShareIntent(activity, track)
    }

    private fun dismissDialog() {
        val dialog = childFragmentManager.findFragmentByTag(TAG_DIALOG)
        dialog?.let {
            childFragmentManager.commit {
                remove(dialog)
            }
        }
    }

    private fun finishTrackSelection() =
            if (activity is TrackActivity) {
                // For tablet we'll opt to leave the track list on the screen instead of removing it
                // getSupportFragmentManager().popBackStack(TRANSACTION_TRACKS, POP_BACK_STACK_INCLUSIVE);
            } else {
                activity.finish()
            }

    private fun showTrackEditDialog(trackUri: Uri) {
        TrackEditDialogFragment.newInstance(trackUri)
                .show(childFragmentManager, TAG_DIALOG)
    }

    private fun showTrackDeleteDialog(track: Uri) {
        TrackDeleteDialogFragment.newInstance(track)
                .show(childFragmentManager, TAG_DIALOG)
    }

    private fun startGpxFileSelection(param: (Intent) -> Unit) {
        if (versionHelper.isAtLeast(Build.VERSION_CODES.KITKAT)) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE) // Intent.ACTION_OPEN_DOCUMENT has too few results
            intent.type = MIME_TYPE_ANY
            startTypeImport(intent, param)
        } else {
            AlertDialog.Builder(activity)
                    .setTitle("Not implemented ")
                    .setMessage("This feature does not exist pre-KitKat")
                    .create()
                    .show()
        }
    }

    private fun startGpxDirectorySelection(param: (Intent) -> Unit) {
        if (versionHelper.isAtLeast(Build.VERSION_CODES.LOLLIPOP)) {
            val intent = Intent(ACTION_OPEN_DOCUMENT_TREE)
            startTypeImport(intent, param)
        } else {
            AlertDialog.Builder(activity)
                    .setTitle("Not implemented ")
                    .setMessage("This feature does not exist pre-Lollipop")
                    .create()
                    .show()
        }
    }

    private fun startTypeImport(intent: Intent, param: (Intent) -> Unit) {
        if (activity is ActivityResultLambda) {
            activity.startActivityForResult(intent) { resultIntent ->
                resultIntent?.let {
                    ImportTrackTypeDialogFragment().show(childFragmentManager, TAG_DIALOG) { type ->
                        resultIntent.putExtra(KEY_META_FIELD_TRACK_TYPE, type)
                        val takeFlags = resultIntent.flags and Intent.FLAG_GRANT_READ_URI_PERMISSION
                        val uri = resultIntent.data
                        val resolver = activity.contentResolver
                        if (uri != null && intent.action.fromStorageFramework()) {
                            resolver.takePersistableUriPermission(uri, takeFlags)
                        }
                        param(resultIntent)
                    }
                }
            }
        }
    }

    private fun startExporter() {
        val intent = Intent(activity, ExportActivity::class.java)
        activity.startActivity(intent)
    }

    private fun showSharePicker(trackUri: Uri) {
        shareChoiceFragment(trackUri)
                .show(childFragmentManager, TAG_DIALOG)
    }
}

private fun String?.fromStorageFramework() =
        this == ACTION_OPEN_DOCUMENT || this == ACTION_OPEN_DOCUMENT_TREE
