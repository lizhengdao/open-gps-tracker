/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.recording

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.net.Uri
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.content.ContentController
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.ng.base.common.onMainThread
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.excellent
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.high
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.low
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.medium
import nl.renedegroot.android.gpstracker.ng.features.recording.RecordingView.SignalQualityLevel.none
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractPresenter
import nl.renedegroot.android.gpstracker.service.util.LoggingStateController
import nl.renedegroot.android.gpstracker.service.util.LoggingStateListener
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_LOGGING
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_PAUSED
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_STOPPED
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

class RecordingPresenter :
        AbstractPresenter(),
        ContentController.Listener,
        GpsStatusController.Listener,
        LoggingStateListener {

    internal val viewModel = RecordingView(null)

    @Inject
    lateinit var gpsStatusController: GpsStatusController

    @Inject
    lateinit var contentController: ContentController

    @Inject
    lateinit var packageManager: PackageManager

    @Inject
    lateinit var loggingStateController: LoggingStateController

    @Inject
    lateinit var summaryManager: SummaryManager

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override suspend fun onStart() {
        super.onStart()
        loggingStateController.connect(this)
        summaryManager.start()
    }

    @WorkerThread
    override suspend fun onChange() {
        val trackUri = loggingStateController.trackUri
        val state = loggingStateController.loggingState
        val isRecording = (state == STATE_LOGGING) || (state == STATE_PAUSED)
        viewModel.isRecording.set(isRecording)
        when (state) {
            STATE_LOGGING -> viewModel.state.set(R.string.state_logging)
            STATE_PAUSED -> viewModel.state.set(R.string.state_paused)
            STATE_STOPPED -> viewModel.state.set(R.string.state_stopped)
        }
        if (isRecording) {
            startContentUpdates()
            startGpsUpdates()
            readTrackSummary(trackUri)
        } else {
            stopContentUpdates()
            stopGpsUpdates()
        }

    }

    override suspend fun onStop() {
        super.onStop()
        loggingStateController.disconnect()
        stopContentUpdates()
        stopGpsUpdates()
        summaryManager.stop()
    }

    //region View

    fun didSelectSignal() {
        val intent = packageManager.getLaunchIntentForPackage(GPS_STATUS_PACKAGE_NAME)
        if (intent == null) {
            viewModel.navigation.value = Navigation.GpsStatusAppInstallHint
        } else {
            viewModel.navigation.value = Navigation.GpsStatusAppOpen
        }
    }

    fun didSelectSummary() {
        loggingStateController.trackUri?.let {
            viewModel.navigation.value = Navigation.Summary(it)
        }
    }

    //endregion

    //region Service connection

    override fun didStartLogging(trackUri: Uri) = onMainThread {
        viewModel.trackUri.set(trackUri)
        contentController.registerObserver(this, trackUri)
        markDirty()
    }

    override fun didPauseLogging(trackUri: Uri) = onMainThread {
        viewModel.trackUri.set(trackUri)
        markDirty()
    }

    override fun didStopLogging() = onMainThread {
        viewModel.trackUri.set(null)
        contentController.unregisterObserver()
        markDirty()
    }
    //endregion

    //region ContentController

    override fun onChangeUriContent(contentUri: Uri, changesUri: Uri) {
        readTrackSummary(contentUri)
    }

    private fun readTrackSummary(trackUri: Uri?) {
        if (trackUri == null) {
            viewModel.summary.set(SummaryText(R.string.fragment_recording_summary, 0F, false, 0F, 0))
            viewModel.name.set(null)
        } else {
            summaryManager.collectSummaryInfo(trackUri) { summary ->
                val endTime = summary.waypoints.lastOrNull()?.lastOrNull()?.time
                        ?: System.currentTimeMillis()
                val startTime = summary.waypoints.firstOrNull()?.firstOrNull()?.time
                        ?: System.currentTimeMillis()
                val meterPerSecond = summary.distance / (summary.trackedPeriod / 1000F)
                val isRunners = summary.type.isRunning()
                viewModel.summary.set(SummaryText(R.string.fragment_recording_summary, meterPerSecond, isRunners, summary.distance, endTime - startTime))
                viewModel.name.set(summary.name)
            }
        }
    }

    //endregion

    //region GPS status

    override fun onStartListening() {
        viewModel.isScanning.set(true)
        viewModel.hasFix.set(false)
    }

    override fun onStopListening() {
        viewModel.hasFix.set(false)
        viewModel.isScanning.set(false)
        onChange(0, 0)
    }

    override fun onChange(usedSatellites: Int, maxSatellites: Int) {
        viewModel.currentSatellites.set(usedSatellites)
        viewModel.maxSatellites.set(maxSatellites)
        when {
            usedSatellites >= 10 -> viewModel.signalQuality.set(excellent)
            usedSatellites >= 8 -> viewModel.signalQuality.set(high)
            usedSatellites >= 6 -> viewModel.signalQuality.set(medium)
            usedSatellites >= 4 -> viewModel.signalQuality.set(low)
            else -> viewModel.signalQuality.set(none)
        }
    }


    override fun onFirstFix() {
        viewModel.hasFix.set(true)
        viewModel.signalQuality.set(excellent)
    }

    //endregion

    //region Private

    private fun startContentUpdates() {
        contentController.registerObserver(this, viewModel.trackUri.get())
    }

    private fun stopContentUpdates() {
        contentController.unregisterObserver()
    }

    @SuppressLint("MissingPermission")
    private fun startGpsUpdates() {
        gpsStatusController.startUpdates(this)
    }

    private fun stopGpsUpdates() {
        gpsStatusController.stopUpdates()
    }

    //endregion
}
