package nl.renedegroot.android.gpstracker.ng.features.control

import android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionRequester
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

class ControlNavigation @Inject constructor() {

    internal val step = MutableLiveData<Consumable<ControlNavigationStep>>()

    fun showStopLoggingConfirmation() {
        step.postValue(Consumable(ControlNavigationStep.ConfirmStop))
    }

    fun checkBackgroundPermission() {
        step.postValue(Consumable(ControlNavigationStep.CheckBackgroundPermission))
    }
}

class ControlNavigator(
        private val fragment: Fragment,
        private val presenter: ControlPresenter,
        private val permissionRequester: PermissionRequester
) {
    fun observe(controlNavigation: ControlNavigation) {
        controlNavigation.step.observe(fragment.viewLifecycleOwner, Observer { consumable ->
            consumable.consume { navigate(it) }
        })
    }

    private fun navigate(step: ControlNavigationStep) {
        when (step) {
            ControlNavigationStep.ConfirmStop -> {
                AlertDialog.Builder(fragment.requireContext())
                        .setTitle(R.string.control_confirm_stop_title)
                        .setNegativeButton(R.string.control_confirm_stop_negative) { _, _ -> }
                        .setPositiveButton(R.string.control_confirm_stop_positive) { _, _ -> presenter.stopLogging() }
                        .create()
                        .show()
            }
            ControlNavigationStep.CheckBackgroundPermission -> {
                permissionRequester.start(
                        fragment,
                        listOf(ACCESS_FINE_LOCATION, ACCESS_BACKGROUND_LOCATION)
                ) { granted -> presenter.startLoggingWithBackgroundPermission(granted) }
            }
        }
    }
}

internal sealed class ControlNavigationStep {
    object ConfirmStop : ControlNavigationStep()
    object CheckBackgroundPermission : ControlNavigationStep()
}
