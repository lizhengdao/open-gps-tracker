/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.GraphPoint
import java.lang.Math.sqrt
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow

private const val MAX_TIMES_STANDARD_DEVIATION = 2

fun <T> List<T>.standardRange(selector: (T) -> Float): Pair<Float, Float> {
    val mean = this.sumByDouble { selector(it).toDouble() } / this.size
    val sd = sqrt(this.sumByDouble { (selector(it) - mean).pow(2) } / this.size)
    val min = max(0.0, mean - sd * MAX_TIMES_STANDARD_DEVIATION)
    val max = mean + sd * MAX_TIMES_STANDARD_DEVIATION

    return min.toFloat() to max.toFloat()
}

fun List<GraphPoint>.flattenOutliers(): List<GraphPoint> {
    val (min, max) = this.standardRange { it.y }
    fun withinNorm(point: GraphPoint) = point.y > min && point.y < max
    return this.mapIndexed { i, point ->
        if (withinNorm(point)) {
            point
        } else {
            this.neighboursAverage(i)
        }
    }
}

fun List<GraphPoint>.smooth(span: Float) =
        this.mapIndexed { i, point ->
            val ySmooth = this.localAverage(i, span / 2F)
            GraphPoint(point.x, ySmooth.toFloat())
        }

fun <T> List<T>.condense(together: (T, T) -> Boolean, transform: (List<T>) -> T): List<T> {
    val result = mutableListOf<T>()
    var rangeStart = 0
    forEachIndexed { index, item ->
        if (!together(this[rangeStart], item)) {
            result.add(transform(subList(rangeStart, index)))
            rangeStart = index
        }
    }
    val tail = subList(rangeStart, size)
    if (tail.isNotEmpty()) {
        result.add(transform(tail))
    }
    return result
}

private fun List<GraphPoint>.neighboursAverage(pivot: Int, span: Int = 4, allowed: (GraphPoint) -> Boolean = { true }): GraphPoint {
    fun List<GraphPoint>.average(x: Float) =
            GraphPoint(x, (sumByDouble { it.y.toDouble() } / size).toFloat())

    val from = max(pivot - span, 0)
    val to = min(pivot + span, lastIndex)
    val localPoints = subList(from, to).filter { allowed(it) }

    return localPoints.average(this[pivot].x)
}

private fun List<GraphPoint>.localAverage(pivot: Int, halfSpan: Float): Double {
    val collectedPoints = mutableListOf<GraphPoint>()
    for (i in pivot downTo 0) {
        if (this[pivot].x - this[i].x < halfSpan) {
            collectedPoints.add(this[i])
        } else {
            break
        }
    }
    for (i in pivot + 1 until this.size) {
        if (this[i].x - this[pivot].x < halfSpan) {
            collectedPoints.add(this[i])
        } else {
            break
        }
    }

    return collectedPoints.sumByDouble { it.y.toDouble() } / collectedPoints.size
}
