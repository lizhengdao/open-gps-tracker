/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.app.JobIntentService
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.VALUE_TYPE_DEFAULT
import nl.renedegroot.android.opengpstrack.ng.features.R
import timber.log.Timber
import javax.inject.Inject

class ImportService : JobIntentService() {

    @Inject
    lateinit var importController: GpxImportController
    @Inject
    lateinit var context: Context

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    companion object {

        private const val EXTRA_FILE = "GPX_FILE_URI"
        private const val EXTRA_TYPE = "EXTRA_TRACK_TYPE"
        private const val EXTRA_DIRECTORY = "GPX_DIRECTORY_URI"
        private val JOB_ID = R.menu.import_export

        fun importFile(uri: Uri, trackType: String = VALUE_TYPE_DEFAULT) {
            val work = Intent()
            work.putExtra(EXTRA_FILE, uri)
            work.putExtra(EXTRA_TYPE, trackType)
            enqueueWork(BaseConfiguration.baseComponent.applicationContext(), ImportService::class.java, JOB_ID, work)
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        fun importDirectory(uri: Uri, trackType: String = VALUE_TYPE_DEFAULT) {
            val work = Intent()
            work.putExtra(EXTRA_DIRECTORY, uri)
            work.putExtra(EXTRA_TYPE, trackType)
            enqueueWork(BaseConfiguration.baseComponent.applicationContext(), ImportService::class.java, JOB_ID, work)
        }
    }

    @SuppressLint("NewApi")
    override fun onHandleWork(intent: Intent) {
        val trackType = intent.getStringExtra(EXTRA_TYPE)
        when {
            intent.hasExtra(EXTRA_FILE) -> importController.import(intent.getParcelableExtra(EXTRA_FILE), trackType)
            intent.hasExtra(EXTRA_DIRECTORY) -> importController.importDirectory(intent.getParcelableExtra(EXTRA_DIRECTORY), trackType)
            else -> Timber.e("Failed to handle import work $intent")
        }
    }
}
