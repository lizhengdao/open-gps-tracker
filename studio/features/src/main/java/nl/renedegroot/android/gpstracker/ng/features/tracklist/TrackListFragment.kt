/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.app.SearchManager
import android.content.Context
import android.database.CursorWrapper
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.utils.activityresult.ActivityResultLambdaFragment
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionRequester
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentTracklistBinding
import javax.inject.Inject

/**
 * Sets up display and selection of tracks in a list style
 */
class TrackListFragment : ActivityResultLambdaFragment() {

    lateinit var presenter: TrackListPresenter

    @Inject
    lateinit var permissionRequester: PermissionRequester

    private var binding: FragmentTracklistBinding? = null

    companion object {
        fun newInstance(): TrackListFragment {
            return TrackListFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ViewModelProvider(this).get(TrackListPresenter::class.java)
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<FragmentTracklistBinding>(inflater, R.layout.fragment_tracklist, container, false)
        binding.fragmentTracklistList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        val itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        itemAnimator.supportsChangeAnimations = false
        binding.fragmentTracklistList.itemAnimator = itemAnimator
        binding.presenter = presenter
        binding.viewModel = presenter.viewState
        TrackListNavigator(requireActivity(), childFragmentManager).observe(this.viewLifecycleOwner, presenter.navigation)
        this.binding = binding

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        setHasOptionsMenu(true)
        permissionRequester.start(this) {
            presenter.start()
        }
    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is ShareChoiceFragment) {
            fragment.configure(presenter)
        }
    }

    override fun onStop() {
        super.onStop()
        setHasOptionsMenu(false)
        permissionRequester.stop(this) {
            presenter.stop()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.search, menu)
        inflater.inflate(R.menu.import_export, menu)

        attachSearch(menu.findItem(R.id.action_search))
        presenter.onSearchClosed()
    }

    private fun attachSearch(searchItem: MenuItem) {
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = searchItem.actionView as SearchView?
        searchView?.setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
        searchView?.setOnSuggestionListener(object : SearchView.OnSuggestionListener {
            override fun onSuggestionSelect(position: Int): Boolean {
                setQueryToSuggestion(position, false)
                return false
            }

            override fun onSuggestionClick(position: Int): Boolean {
                setQueryToSuggestion(position, true)
                return false
            }

            private fun setQueryToSuggestion(position: Int, submit: Boolean) {
                val suggestionsAdapter: androidx.cursoradapter.widget.CursorAdapter? = searchView.suggestionsAdapter
                val item = suggestionsAdapter?.getItem(position) as CursorWrapper?
                val selected = item?.getString(1)
                searchView.setQuery(selected, submit)
            }
        })
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                presenter.onSearchOpened()
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                presenter.onSearchClosed()
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                R.id.menu_item_export -> {
                    presenter.didSelectExportToDirectory()
                    true
                }
                R.id.menu_item_import -> {
                    presenter.didSelectImportFromDirectory()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionRequester.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
    }

}
