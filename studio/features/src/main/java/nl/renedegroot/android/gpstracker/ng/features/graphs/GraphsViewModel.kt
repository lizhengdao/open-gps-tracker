/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.graphs

import android.net.Uri
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.GraphDataCalculator

class GraphsViewModel {
    val trackUri = ObservableField<Uri?>()

    val startTime = ObservableField<Long>(0L)
    val timeSpan = ObservableField<Long>(0L)
    val paused = ObservableField<Long>(0)

    val distance = ObservableField<Float>(0F)
    val speed = ObservableField<Float>(0F)
    val maxSpeed = ObservableField<Float>(0F)
    val waypoints = ObservableField<String>("-")

    val distanceSelected = ObservableBoolean(false)
    val durationSelected = ObservableBoolean(true)
    val inverseSpeed = ObservableBoolean(false)

    val graphSpeedDataProvider = ObservableField<GraphDataCalculator>(GraphDataCalculator.DefaultGraphValueDescriptor)
    val graphAltitudeDataProvider = ObservableField<GraphDataCalculator>(GraphDataCalculator.DefaultGraphValueDescriptor)
}
