package nl.renedegroot.android.gpstracker.ng.features.appshortcut

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.gpstracker.service.util.LoggingStateController
import nl.renedegroot.android.gpstracker.service.util.LoggingStateListener
import nl.renedegroot.android.gpstracker.utils.preference.LoggingSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.preference.activeInterval
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractPresenter
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

class StartLoggingPresenter(
        private val loggingSettingsPreferences: LoggingSettingsPreferences,
        private val serviceCommander: ServiceCommander
) : AbstractPresenter(), LoggingStateListener {

    private var isConnected = false
        set(value) {
            markDirty()
            field = value
        }
    private var isDisplayed = false
        set(value) {
            markDirty()
            field = value
        }

    val shouldFinish = MutableLiveData<Boolean>()

    @Inject
    lateinit var loggingStateController: LoggingStateController

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override suspend fun onFirstStart() {
        super.onFirstStart()
        withContext(Dispatchers.IO) {
            val interval = loggingSettingsPreferences.activeInterval
            serviceCommander.startGPSLogging(R.string.initial_track_name, interval)
        }
    }

    override suspend fun onStart() {
        loggingStateController.connect(this)
    }

    override suspend fun onStop() {
        loggingStateController.disconnect()
    }

    fun onDisplay() {
        isDisplayed = true
    }

    override suspend fun onChange() {
        if (isConnected && isDisplayed) {
            delay(4000)
            shouldFinish.postValue(true)
        }
    }

    override fun didStopLogging() {
        isConnected = true
    }

    override fun didPauseLogging(trackUri: Uri) {
        isConnected = true
    }

    override fun didStartLogging(trackUri: Uri) {
        isConnected = true

    }
}