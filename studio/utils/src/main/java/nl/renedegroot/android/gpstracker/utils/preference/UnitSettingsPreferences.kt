package nl.renedegroot.android.gpstracker.utils.preference

import android.content.Context
import android.content.SharedPreferences
import nl.renedegroot.android.gpstracker.utils.formatting.LocaleProvider
import nl.renedegroot.android.gpstracker.utils.getAsFloat
import nl.renedegroot.android.test.utils.R

private const val SETTINGS_UNIT_PREFERENCE = "unit_settings"

private const val SETTING_UNIT = "unit"
private val SETTINGS_UNIT_DEFAULT = UnitSystem.Device.name

private const val SETTINGS_INVERSE_RUNNING = "inverse running"
private const val SETTINGS_INVERSE_RUNNING_DEFAULT = true

private val imperialCountries = listOf("GB", "US")

class UnitSettingsPreferences(
        private val context: Context,
        private val localeProvider: LocaleProvider = LocaleProvider(context)) {

    private val preferences by lazy { context.getSharedPreferences(SETTINGS_UNIT_PREFERENCE, Context.MODE_PRIVATE) }
    private var unitPreference by stringPreference(context, SETTINGS_UNIT_PREFERENCE, SETTING_UNIT, SETTINGS_UNIT_DEFAULT)
    private val preferenceChangeListener = object : SharedPreferences.OnSharedPreferenceChangeListener {
        var onChanges: (() -> Unit)? = null
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            onChanges?.invoke()
        }
    }

    var useInverseRunningSpeed by booleanPreference(context, SETTINGS_UNIT_PREFERENCE, SETTINGS_INVERSE_RUNNING, SETTINGS_INVERSE_RUNNING_DEFAULT)
    var unitSystem: UnitSystem
        get() = UnitSystem.valueOf(unitPreference ?: SETTINGS_UNIT_DEFAULT)
        set(value) {
            unitPreference = value.name
        }
    val displayCountry = checkNotNull(localeProvider.locale.displayCountry)
    val isImperialCountry get() = imperialCountries.contains(localeProvider.locale.country)
    val useMetrics: Boolean
        get() = when (unitSystem) {
            UnitSystem.Metric -> true
            UnitSystem.Imperial -> false
            UnitSystem.Device -> !isImperialCountry
        }
    val meterPerSecondToSpeed
        get() = if (useMetrics) {
            context.resources.getAsFloat(R.string.units__metrics_mps_to_speed)
        } else {
            context.resources.getAsFloat(R.string.units__imperial_mps_to_speed)
        }
    val meterToSmallDistance
        get() = if (useMetrics) {
            context.resources.getAsFloat(R.string.units__metrics_m_to_small_distance)
        } else {
            context.resources.getAsFloat(R.string.units__imperial_m_to_small_distance)
        }
    val mpsToSpeed
        get() = if (useMetrics) {
            context.resources.getAsFloat(R.string.units__metrics_mps_to_speed)
        } else {
            context.resources.getAsFloat(R.string.units__imperial_mps_to_speed)
        }
    val speedUnit: String
        get() = if (useMetrics) {
            context.resources.getString(R.string.units__metrics_speed_unit)
        } else {
            context.resources.getString(R.string.units__imperial_speed_unit)
        }
    val meterToBigDistance
        get() = if (useMetrics) {
            context.resources.getAsFloat(R.string.units__metrics_m_to_big_distance)
        } else {
            context.resources.getAsFloat(R.string.units__imperial_m_to_big_distance)
        }

    fun observeChanges(function: () -> Unit) {
        preferenceChangeListener.onChanges = function
        preferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    fun ignoreChanges() {
        preferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
    }
}

enum class UnitSystem {
    Device,
    Metric,
    Imperial;

    override fun toString() = "Units from $name"
}
