/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.utils.permissions

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionChecker
import nl.renedegroot.android.gpstracker.ng.base.permissions.PermissionRequester

fun permissionActivityIntent(
        context: Context,
        permissions: ArrayList<String>,
        pendingIntent: PendingIntent
) =
        Intent(context, PermissionActivity::class.java).also {
            it.putExtras(bundleOf(
                    ARGUMENT_PERMISSIONS to permissions,
                    ARGUMENT_INTENT to pendingIntent
            ))
        }

class PermissionActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(android.R.id.content, permissionFragment(
                            intent.getStringArrayListExtra(ARGUMENT_PERMISSIONS) as ArrayList<String>,
                            intent.getParcelableExtra(ARGUMENT_INTENT) as PendingIntent
                    ))
                    .commit()
        }
    }

    class PermissionFragment : Fragment() {

        private val permissionRequester = PermissionRequester(PermissionChecker())
        private val permission: List<String>
            get() = checkNotNull(requireArguments().getStringArrayList(ARGUMENT_PERMISSIONS))
        private val pendingIntent: PendingIntent
            get() = checkNotNull(requireArguments().getParcelable(ARGUMENT_INTENT))

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
                FrameLayout(requireContext())

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            permissionRequester.start(this, permission) {
                pendingIntent.send(requireContext(), 0, null)
                requireActivity().finish()
            }
        }

        override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
            permissionRequester.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
        }
    }
}

private const val ARGUMENT_PERMISSIONS = "ARGUMENT_PERMISSIONS"
private const val ARGUMENT_INTENT = "ARGUMENT_INTENT"

private fun permissionFragment(
        permissions: ArrayList<String>,
        pendingIntent: PendingIntent
) =
        PermissionActivity.PermissionFragment().also {
            it.arguments = bundleOf(
                    ARGUMENT_PERMISSIONS to permissions,
                    ARGUMENT_INTENT to pendingIntent
            )
        }
