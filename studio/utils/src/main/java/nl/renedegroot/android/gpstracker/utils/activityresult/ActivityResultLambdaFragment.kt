/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.utils.activityresult

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import android.util.SparseArray
import timber.log.Timber

/**
 * Default implementation for ActivityResultLambda on a V4 Fragment super class
 */
abstract class ActivityResultLambdaFragment : Fragment(), ActivityResultLambda {

    //region Activity results

    private var requests = 1
    private val resultHandlers = SparseArray<(Intent?) -> Unit>()

    override fun startActivityForResult(intent: Intent, resultHandler: (Intent?) -> Unit) {
        val requestCode = ++requests
        resultHandlers.put(requestCode, resultHandler)
        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, result: Intent?) {
        val resultHandler = resultHandlers.get(requestCode)
        resultHandlers.remove(requestCode)
        if (resultCode == Activity.RESULT_OK && resultHandler != null) {
            resultHandler(result)
        } else {
            Timber.e("Received $result without an way to handle")
            super.onActivityResult(requestCode, resultCode, result)
        }
    }

    //endregion
}
